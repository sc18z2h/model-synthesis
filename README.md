Application Download Links:  
[Download (windows)](https://gitlab.com/sc18z2h/model-synthesis-download/-/raw/master/model%20synthesis%20(windows).zip)  
[Download (mac)](https://gitlab.com/sc18z2h/model-synthesis-download/-/raw/master/model%20synthesis%20(mac).zip)  
[Download (linux)](https://gitlab.com/sc18z2h/model-synthesis-download/-/raw/master/model%20synthesis%20(linux).zip)  

If you want to play with the source code, just download this repository and open it in Unity.
