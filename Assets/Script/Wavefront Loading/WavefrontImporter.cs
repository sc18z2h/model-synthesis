using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public static class WavefrontImporter
{
	[Serializable]
	public struct WavefrontShader
    {
		public Shader shader;

		[SerializeField]
		private string
			diffuseMapName,
			specularHighlightMapName,
			bumpMapName,
			displacementMapName,

			specularHighlightMapKeyword,
			bumpMapKeyword,
			displacementMapKeyword;

		[HideInInspector]
		[SerializeField]
		public int
			diffuseMap,
			specularHighlightMap,
			bumpMap,
			displacementMap;

		public static WavefrontShader Standard
        {
			get
			{
				WavefrontShader standard = new WavefrontShader()
				{
					shader = null,
					diffuseMapName = "_MainTex",
					specularHighlightMapName = "_MetallicGlossMap",
					bumpMapName = "_BumpMap",
					displacementMapName = "_ParallaxMap",

					specularHighlightMapKeyword = "_METALLICGLOSSMAP",
					bumpMapKeyword = "_NORMALMAP",
					displacementMapKeyword = "_PARALLAXMAP"
				};
				standard.UpdateIds();
				return standard;
			}
		}

		public void UpdateIds()
        {
			diffuseMap = Shader.PropertyToID(diffuseMapName);
			specularHighlightMap = Shader.PropertyToID(specularHighlightMapName);
			bumpMap = Shader.PropertyToID(bumpMapName);
			displacementMap = Shader.PropertyToID(displacementMapName);
		}

		public void UpdateMaterial(
			Material material,
			Texture diffuseTexture, Texture specularHighlightTexture, Texture bumpTexture, Texture displacementTexture)
        {
			void SetTexture(Material material, Texture texture, int nameId, string keyword)
            {
				if (0 < keyword.Length)
				{
					if (texture == null)
						material.DisableKeyword(keyword);
					else
						material.EnableKeyword(keyword);
				}
				material.SetTexture(nameId, texture);
            }
			SetTexture(material, diffuseTexture, diffuseMap, string.Empty);
			SetTexture(material, specularHighlightTexture, specularHighlightMap, specularHighlightMapKeyword);
			SetTexture(material, bumpTexture, bumpMap, bumpMapKeyword);
			SetTexture(material, displacementTexture, displacementMap, displacementMapKeyword);
		}
	}

	private struct BuildingMaterial
    {
		public Material material;
		public List<int> triangles;
		public BuildingMaterial(Material material)
        {
			this.material = material;
			triangles = new List<int>();
        }
    }

	private enum FaceType { unknown, position, positionTexture, positionTextureNormal, positionNormal };

	private static string MessageWithLine(string message, string line, int lineNumber)
	{
		const string formatter = "{0}: {1} ({2})";
		return string.Format(formatter, message, line, lineNumber);
	}

	private static Vector3 ParseVector3(string[] tokens, string line, int lineNumber)
	{
		if (tokens.Length < 4)
			throw new InvalidDataException(MessageWithLine("declaration invalid - requires 3 elements", line, lineNumber));
		else if (float.TryParse(tokens[1], out float x) &&
			float.TryParse(tokens[2], out float y) &&
			float.TryParse(tokens[3], out float z))
			return new Vector3(x, y, z);
		else
			throw new InvalidDataException(MessageWithLine("must use floating values", line, lineNumber));
	}

	private static Vector2 ParseVector2(string[] tokens, string line, int lineNumber)
	{
		if (tokens.Length < 3)
			throw new InvalidDataException(MessageWithLine("declaration invalid - requires 2 elements", line, lineNumber));
		else if (float.TryParse(tokens[1], out float x) &&
			float.TryParse(tokens[2], out float y))
			return new Vector2(x, y);
		else
			throw new InvalidDataException(MessageWithLine("must use floating values", line, lineNumber));
	}

	private static float ParseFloat(string[] tokens, string line, int lineNumber)
    {
		if (tokens.Length < 2)
			throw new InvalidDataException(MessageWithLine("declaration invalid - requires 1 element", line, lineNumber));
		else if (float.TryParse(tokens[1], out float x))
			return x;
		else
			throw new InvalidDataException(MessageWithLine("must use floating values", line, lineNumber));
	}

	private static FaceType ParseVertex(string vertex, List<int> indices)
	{
		indices.Clear();

		int lastSlashIndex = -1;
		int slashesCount = 0;
		for (int i = 0; i < vertex.Length + 1; ++i)
			if (i == vertex.Length || vertex[i] == '/')
			{
				if (int.TryParse(vertex.Substring(lastSlashIndex + 1, i - lastSlashIndex - 1), out int number))
				{
					indices.Add(number);

					if (i < vertex.Length && vertex[i] == '/')
					{
						lastSlashIndex = i;
						++slashesCount;
					}
				}
				else
				{
					throw new InvalidDataException("Face indices not an integer index: " + vertex);
				}
			}

		switch(slashesCount)
        {			
			case 0:
				return FaceType.position;
			case 1:
				return FaceType.positionTexture;
			case 2:
				if (vertex[lastSlashIndex - 1] == '/')
					return FaceType.positionNormal;
				else
					return FaceType.positionTextureNormal;
			default:
				return FaceType.unknown;
		}
	}

	private static void ImportMtl(
		string folder, string mtlFile,
		Dictionary<string, BuildingMaterial> buildingMaterials,
		WavefrontShader wavefrontShader)
	{
		Material currentMaterial = null;
		int lineNumber = 0;

		foreach (string line in File.ReadLines(folder + '/' + mtlFile))
		{
			++lineNumber;
			if (line.StartsWith("#") || string.IsNullOrWhiteSpace(line))
				continue;

			//materialMap
			string[] tokens = line.Split(' ');
			if (tokens.Length == 0)
				continue;

			if (tokens[0] == "newmtl")
			{
				string materialName = line.Substring(7);
				if (buildingMaterials.ContainsKey(materialName) == false)
				{
					currentMaterial = new Material(wavefrontShader.shader);
					currentMaterial.name = materialName;
					buildingMaterials.Add(materialName, new BuildingMaterial(currentMaterial));					
				}
			}
			else
			{
				if (currentMaterial == null)
					throw new InvalidDataException(MessageWithLine("no material declared with newmtl", line, lineNumber));

				else if (tokens[0] == "map_Kd")
					currentMaterial.SetTexture(wavefrontShader.diffuseMap, ImportTexture(folder + '/' + line.Substring(7)));
				else if (tokens[0] == "map_Ns")
					currentMaterial.SetTexture(wavefrontShader.specularHighlightMap, ImportTexture(folder + '/' + line.Substring(7)));
				else if (tokens[0] == "map_bump")
					currentMaterial.SetTexture(wavefrontShader.bumpMap, ImportTexture(folder + '/' + line.Substring(9)));
				else if (tokens[0] == "bump")
					currentMaterial.SetTexture(wavefrontShader.bumpMap, ImportTexture(folder + '/' + line.Substring(5)));
				else if (tokens[0] == "map_Ns")
					currentMaterial.SetTexture(wavefrontShader.displacementMap, ImportTexture(folder + '/' + line.Substring(7)));
			}
		}
	}

	public delegate void ImportTextureEvent(Texture2D texture);
	public static event ImportTextureEvent OnImportTexture;

	public static Texture2D ImportTexture(string path)
	{
		Texture2D texture = new Texture2D(0, 0);
		if (texture.LoadImage(File.ReadAllBytes(path)))
		{
			texture.name = Path.GetFileName(path);
			OnImportTexture?.Invoke(texture);
			return texture;
		}
		else
			throw new FileLoadException("couldn't create texture: " + path);
	}

	private static bool SetFinalList<T>(List<T> declaredList, List<T> finalList, int index)
    {
		// wavefront index is 1-based		
		if (1 <= index && index <= declaredList.Count)
		{
			finalList.Add(declaredList[index - 1]);
			return false;
		}
		else
			return true;
	}

	public static void ImportModel(
		string path, float scale, bool flipUVs,
		MeshFilter intoMeshFilter, Renderer intoRenderer,
		WavefrontShader wavefrontShader)
	{
		if (path == null)
			throw new ArgumentNullException("path");
		if (path.EndsWith(".obj") == false)
			throw new ArgumentException("file must be of type \".obj\"", "path");
		
		string folder = Path.GetDirectoryName(path);
		if (folder.Length == 0)
			folder = ".";

		List<Vector3>
			declaredPositions = new List<Vector3>(),
			declaredNormals = new List<Vector3>(),
			finalPositions = new List<Vector3>(),
			finalNormals = new List<Vector3>();
		List<Vector2>
			declaredUVs = new List<Vector2>(),
			finalUVs = new List<Vector2>();

		Dictionary<string, BuildingMaterial> buildingMaterials = new Dictionary<string, BuildingMaterial>();
		// start as unassigned
		BuildingMaterial defaultBuildingMaterial = new BuildingMaterial();
		BuildingMaterial currentBuildingMaterial = new BuildingMaterial();
		FaceType usingFaceType = FaceType.unknown;

		List<int> indices = new List<int>();

		int lineNumber = 0;
		foreach (string line in File.ReadLines(path))
		{
			++lineNumber;
			if (line.StartsWith("#") || string.IsNullOrWhiteSpace(line))
				continue;

			string[] tokens = line.Split(' ');
			if (tokens.Length == 0)
				continue;

			if (tokens[0] == "f")
			{
				if (4 != tokens.Length)
					throw new InvalidDataException(MessageWithLine("only 3 sided faces supported", line, lineNumber));
				if (currentBuildingMaterial.triangles == null)
				{
					// assign default material
					Material defaultMaterial = new Material(wavefrontShader.shader);
					defaultMaterial.name = "default material";
					defaultBuildingMaterial = currentBuildingMaterial = new BuildingMaterial(defaultMaterial);
				}

				for (int vertex = 0; vertex < 3; ++vertex)
				{
					FaceType faceType;
					try
					{
						faceType = ParseVertex(tokens[vertex + 1], indices);
					}
					catch (Exception exception)
					{
						throw new InvalidDataException(MessageWithLine("face declaration invalid", line, lineNumber), exception);
					}

					if (faceType == FaceType.unknown)
						throw new InvalidDataException(MessageWithLine("face declaration invalid", line, lineNumber));

					if (usingFaceType == FaceType.unknown)
						usingFaceType = faceType;
					else if (usingFaceType != faceType)
						throw new InvalidDataException(MessageWithLine("inconsistent face declaration types", line, lineNumber));

					switch (usingFaceType)
					{
						case FaceType.position:
							// f v1 v2 v3
							if (SetFinalList(declaredPositions, finalPositions, indices[0]))
								throw new InvalidDataException(MessageWithLine("face declaration invalid: index out of range", line, lineNumber));
							break;
						case FaceType.positionTexture:
							// f v1/t1 v2/t2 v3/t3
							if (SetFinalList(declaredPositions, finalPositions, indices[0]) ||
								SetFinalList(declaredUVs, finalUVs, indices[1]))
								throw new InvalidDataException(MessageWithLine("face declaration invalid: index out of range", line, lineNumber));
							break;
						case FaceType.positionNormal:
							// f v1//vn1 v2//vn2 v3//vn3
							if (SetFinalList(declaredPositions, finalPositions, indices[0]) ||
								SetFinalList(declaredNormals, finalNormals, indices[1]))
								throw new InvalidDataException(MessageWithLine("face declaration invalid: index out of range", line, lineNumber));
							break;
						case FaceType.positionTextureNormal:
							// f v1/vt1/vn1 v2/vt2/vn2 v3/vt3/vn3
							if (SetFinalList(declaredPositions, finalPositions, indices[0]) ||
								SetFinalList(declaredUVs, finalUVs, indices[1]) ||
								SetFinalList(declaredNormals, finalNormals, indices[2]))
								throw new InvalidDataException(MessageWithLine("face declaration invalid: index out of range", line, lineNumber));
							break;
					}
					// doesn't detect repeating vertices, because it relies on Mesh.Optimize.
					currentBuildingMaterial.triangles.Add(finalPositions.Count - 1);
				}
			}
			else if (tokens[0] == "v")
				// vertex position data
				// v x y z [w]
				declaredPositions.Add(ParseVector3(tokens, line, lineNumber) * scale);
			else if (tokens[0] == "vt")
			{
				float y = 0;
				// texture coordinates
				// vt u [v [w]]
				if (tokens.Length < 2)
					throw new InvalidDataException(MessageWithLine("invalid texture declaration", line, lineNumber));
				else if (float.TryParse(tokens[1], out float x) &&
					(tokens.Length < 3 || float.TryParse(tokens[2], out y)))
					declaredUVs.Add(new Vector2(x, flipUVs ? 1 - y : y));
				else
					throw new InvalidDataException(MessageWithLine("texture must have floating values", line, lineNumber));
			}
			else if (tokens[0] == "vn")
				// vertex normals
				// vn x y z
				declaredNormals.Add(ParseVector3(tokens, line, lineNumber));
			else if (tokens[0] == "mtllib")
			{
				try
				{
					ImportMtl(folder, line.Substring(7), buildingMaterials, wavefrontShader);
				}
				catch (Exception exception)
				{
					throw new InvalidDataException(MessageWithLine("couldn't read mtl file", line, lineNumber), exception);
				}
			}
			else if (tokens[0] == "usemtl")
			{
				if (buildingMaterials.TryGetValue(line.Substring(7), out BuildingMaterial material))
					currentBuildingMaterial = material;
			}
		}

		if(65535 < finalPositions.Count)
			throw new Exception("meshes may not have more than 65535 vertices");

		Material[] materials;
		Mesh mesh = new Mesh();
		mesh.name = Path.GetFileNameWithoutExtension(path);		
		mesh.vertices = finalPositions.ToArray();
		mesh.normals = finalNormals.ToArray();
		mesh.uv = finalUVs.ToArray();

		int subMeshIndex;
		int subMeshCount;
		if (defaultBuildingMaterial.triangles == null)
		{
			subMeshIndex = 0;
			mesh.subMeshCount = subMeshCount = buildingMaterials.Count;
			materials = new Material[subMeshCount];
		}
		else
		{
			subMeshIndex = 1;
			mesh.subMeshCount = subMeshCount = buildingMaterials.Count + 1;
			mesh.SetTriangles(defaultBuildingMaterial.triangles, 0);
			materials = new Material[subMeshCount];
			materials[0] = defaultBuildingMaterial.material;
		}
		
		foreach(var kvp in buildingMaterials)
        {
			mesh.SetTriangles(kvp.Value.triangles, subMeshIndex);
			materials[subMeshIndex] = kvp.Value.material;
			++subMeshIndex;
        }
		mesh.Optimize();

		intoMeshFilter.sharedMesh = mesh;
		intoRenderer.materials = materials;
	}
}
