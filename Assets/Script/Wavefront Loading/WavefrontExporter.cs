using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public static class WavefrontExporter
{
    /// <summary>
    /// Only exports the mesh, not the materials.
    /// </summary>
    public static void ExportModel(string path, Mesh mesh)
    {
        if (mesh == null)
            throw new ArgumentNullException("mesh", "mesh cannot be null");

        List<Vector3> vertices = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();
        List<Vector3> normals = new List<Vector3>();        

        mesh.GetVertices(vertices);
        mesh.GetUVs(0, uvs);
        mesh.GetNormals(normals);

        int subMeshCount = mesh.subMeshCount;
        List<List<int>> subMeshes = new List<List<int>>(subMeshCount);
        for (int i = 0; i < subMeshCount; ++i)
        {
            subMeshes[i] = new List<int>();
            mesh.GetTriangles(subMeshes[i], i);
        }

        ExportModelWithLists(path, mesh.name, vertices, uvs, normals, subMeshes);
    }

    /// <summary>
    /// Requires that all meshes use the same materials array (submeshes match).
    /// </summary>
    public static void ExportModels(string path, List<Mesh> meshes, List<Vector3> offsets)
    {
        if (meshes == null)
            throw new ArgumentNullException("meshes", "meshes cannot be null");
        else if (meshes.Count == 0)
            throw new ArgumentException("meshes cannot be empty", "meshes");
        else if(offsets == null)
            throw new ArgumentNullException("offsets", "offsets cannot be null");
        else if(meshes.Count != offsets.Count)
            throw new ArgumentException("meshes and offsets must have the same length");

        List<Vector3> vertices = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();
        List<Vector3> normals = new List<Vector3>();

        int subMeshCount = meshes[0].subMeshCount;
        List<List<int>> subMeshes = new List<List<int>>(subMeshCount);
        for (int i = 0; i < subMeshCount; ++i)
            subMeshes.Add(new List<int>());

        List<Vector3> getVertices = new List<Vector3>();
        List<Vector2> getUVs = new List<Vector2>();
        List<Vector3> getNormals = new List<Vector3>();
        List<int> getTriangles = new List<int>();

        for (int mesh = 0; mesh < meshes.Count; ++mesh)
        {
            for (int subMesh = 0; subMesh < subMeshCount; ++subMesh)
            {                
                meshes[mesh].GetTriangles(getTriangles, subMesh);
                AddRange(subMeshes[subMesh], getTriangles, vertices.Count);
            }

            meshes[mesh].GetVertices(getVertices);
            meshes[mesh].GetUVs(0, getUVs);
            meshes[mesh].GetNormals(getNormals);

            AddRange(vertices, getVertices, offsets[mesh]);
            AddRange(uvs, getUVs);
            AddRange(normals, getNormals);
        }

        ExportModelWithLists(path, meshes[0].name, vertices, uvs, normals, subMeshes);
    }

    /// <summary>
    /// Avoiding object allocation when iterating with an iterator.
    /// </summary>
    private static void AddRange<T>(List<T> addToList, List<T> addFromList)
    {
        for (int i = 0; i < addFromList.Count; ++i)
            addToList.Add(addFromList[i]);
    }

    private static void AddRange(List<Vector3> addToList, List<Vector3> addFromList, Vector3 offset)
    {
        for (int i = 0; i < addFromList.Count; ++i)
            addToList.Add(addFromList[i] + offset);
    }

    private static void AddRange(List<int> addToList, List<int> addFromList, int offset)
    {
        for (int i = 0; i < addFromList.Count; ++i)
            addToList.Add(addFromList[i] + offset);
    }

    private static void ExportModelWithLists(
        string path, string meshName, 
        List<Vector3> vertices, List<Vector2> uvs, List<Vector3> normals,
        List<List<int>> subMeshes)
    {
        if (string.IsNullOrWhiteSpace(path))
            throw new ArgumentException("path cannot be empty", "path");
        else if(path.EndsWith(".obj") == false)
            throw new ArgumentException("path must end with \".obj\"", "path");

        StreamWriter streamWriter;
        try
        {
            streamWriter = new StreamWriter(File.Open(path, FileMode.Create, FileAccess.Write));
        }
        catch(Exception exception)
        {
            throw new Exception("couldn't open path for writing", exception);
        }

        streamWriter.WriteLine("o " + meshName);
        for(int i = 0; i < vertices.Count; ++i)
            streamWriter.WriteLine(string.Format("v {0:F5} {1:F5} {2:F5}", vertices[i].x, vertices[i].y, vertices[i].z));
        for(int i = 0; i < uvs.Count; ++i)
            streamWriter.WriteLine(string.Format("vt {0:F5} {1:F5}", uvs[i].x, uvs[i].y));
        for (int i = 0; i < normals.Count; ++i)
            streamWriter.WriteLine(string.Format("vn {0:F5} {1:F5} {2:F5}", normals[i].x, normals[i].y, normals[i].z));

        for(int i = 0; i < subMeshes.Count; ++i)
        {
            // minor indication that the material for each submesh should be written in here
            streamWriter.WriteLine("s off");

            List<int> triangles = subMeshes[i];
            for (int j = 0; j < triangles.Count; j += 3)
                // wavefront indices are 1-based
                streamWriter.WriteLine(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}",
                    triangles[j] + 1, triangles[j + 1] + 1, triangles[j + 2] + 1));
        }

        streamWriter.Close();
    }
}
