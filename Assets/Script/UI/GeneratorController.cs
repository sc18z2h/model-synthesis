using UnityEngine;

public class GeneratorController : MonoBehaviour
{
    public GameObject generatorSection;
    public CameraController cameraController;
    public ChunkGenerator chunkGenerator;

    private void OnEnable()
    {
        generatorSection.SetActive(true);
        if (0 < chunkGenerator.chunkPool.chunkPool.Count)
            cameraController.Focus(chunkGenerator.chunkPool.GetBounds());
    }

    private void OnDisable()
    {
        if(generatorSection != null)
            generatorSection.SetActive(false);
    }
}
