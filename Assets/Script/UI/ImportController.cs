using System;
using System.Collections.Generic;
using UnityEngine;
using static WavefrontImporter;

public class ImportController : MonoBehaviour
{
    public delegate void NamesUpdate(List<string> names);
    public event NamesUpdate OnModelNamesUpdate;
    public event NamesUpdate OnTextureNamesUpdate;

    /// <summary>
    /// All indices are 0 based
    /// </summary>
    public delegate void ModelUpdate(int index, int albedo, int metallic, int normal, int height);
    public event ModelUpdate OnCurrentModelUpdate;

    /// <summary>
    /// All indices are 0 based
    /// </summary>
    public delegate void TextureUpdate(int index, int wrap, int filter);
    public event TextureUpdate OnCurrentTextureUpdate;

    [SerializeField]
    private WavefrontShader wavefrontShader = WavefrontShader.Standard;

    [SerializeField]
    private GameObject modelPrefab;
    public List<GameObject> allModels;
    public List<Texture> allTextures;
    [SerializeField]
    private GameObject importSection;
    [SerializeField]
    private CameraController cameraController;

    // 0 based
    private GameObject currentModel;

    // 0 based
    private Texture currentTexture;

    private List<string> namesCache;

    [ContextMenu("Reset wavefrontShader")]
    private void ResetWavefrontShader()
    {
        wavefrontShader = WavefrontShader.Standard;
    }

    private void OnValidate()
    {
        wavefrontShader.UpdateIds();
    }

    private void Awake()
    {
        currentModel = null;
        currentTexture = null;
        namesCache = new List<string>();
        wavefrontShader.UpdateIds();

        void CheckedAdd(Texture texture)
        {
            if (texture != null && allTextures.Contains(texture) == false)
                allTextures.Add(texture);
        }

        for(int i = 0; i < allModels.Count; ++i)
        {
            Material material = allModels[i].GetComponent<Renderer>().sharedMaterial;
            CheckedAdd(material.GetTexture(wavefrontShader.diffuseMap));
            CheckedAdd(material.GetTexture(wavefrontShader.specularHighlightMap));
            CheckedAdd(material.GetTexture(wavefrontShader.bumpMap));
            CheckedAdd(material.GetTexture(wavefrontShader.displacementMap));
        }

        OnImportTexture += MyOnImportTexture;
    }

    private void Start()
    {
        ModelNamesUpdate();
        TextureNamesUpdate();
        CurrentModelUpdate();
        CurrentTextureUpdate();
    }

    private void OnEnable()
    {
        if (currentModel != null)
        {
            currentModel.SetActive(true);
            cameraController.Focus(currentModel);
        }
        importSection.SetActive(true);
    }

    private void OnDisable()
    {
        if (currentModel != null)
            currentModel.SetActive(false);
        if(importSection != null)
            importSection.SetActive(false);
    }

    private void ModelNamesUpdate()
    {
        namesCache.Clear();
        for (int i = 0; i < allModels.Count; ++i)
            namesCache.Add(allModels[i].name);
        OnModelNamesUpdate?.Invoke(namesCache);
    }

    private void TextureNamesUpdate()
    {
        namesCache.Clear();
        for (int i = 0; i < allTextures.Count; ++i)
            namesCache.Add(allTextures[i].name);
        OnTextureNamesUpdate?.Invoke(namesCache);
    }

    private void CurrentModelUpdate()
    {
        if (currentModel == null)
        {
            OnCurrentModelUpdate?.Invoke(-1, -1, -1, -1, -1);
            return;
        }

        Material material = currentModel.GetComponent<Renderer>().sharedMaterial;
        OnCurrentModelUpdate?.Invoke(
            allModels.IndexOf(currentModel),
            allTextures.IndexOf(material.GetTexture(wavefrontShader.diffuseMap)),
            allTextures.IndexOf(material.GetTexture(wavefrontShader.specularHighlightMap)),
            allTextures.IndexOf(material.GetTexture(wavefrontShader.bumpMap)),
            allTextures.IndexOf(material.GetTexture(wavefrontShader.displacementMap)));
    }

    private void CurrentTextureUpdate()
    {
        if(currentTexture == null)
        {
            OnCurrentTextureUpdate?.Invoke(-1, (int)TextureWrapMode.Repeat, (int)FilterMode.Bilinear);
            return;
        }

        OnCurrentTextureUpdate?.Invoke(
            allTextures.IndexOf(currentTexture),
            (int)currentTexture.wrapMode,
            (int)currentTexture.filterMode);
    }

    private void MyOnImportTexture(Texture2D texture)
    {
        allTextures.Add(texture);
        TextureNamesUpdate();
        SwitchTexture(allTextures.Count - 1);
    }

    public string ImportModel(string path, float scale, bool flipUVs)
    {
        GameObject model = Instantiate(modelPrefab, importSection.transform);        
        
        try
        {
            WavefrontImporter.ImportModel(
                path, scale, flipUVs,
                model.GetComponent<MeshFilter>(),
                model.GetComponent<Renderer>(),
                wavefrontShader);
            model.name = model.GetComponent<MeshFilter>().sharedMesh.name;
            allModels.Add(model);
            ModelNamesUpdate();
            SwitchModel(allModels.Count - 1);
            return UIController.RandomSuccessString();
        }
        catch(Exception exception)
        {
            Destroy(model);
            Exception inner = exception.InnerException;
            if (inner == null)
                return exception.Message;
            else
                return string.Format("{0}\n{1}", exception.Message, inner.Message);
        }
    }

    public string ImportTexture(string path)
    {
        try
        {
            WavefrontImporter.ImportTexture(path);
            return UIController.RandomSuccessString();
        }
        catch (Exception exception)
        {
            return exception.Message;
        }
    }

    public void SwitchModel(int index)
    {
        if (index == -1)
        {
            if (currentModel != null)
                currentModel.SetActive(false);

            currentModel = null;
        }
        else if (0 <= index && index < allModels.Count)
        {
            if (currentModel != null)
                currentModel.SetActive(false);

            currentModel = allModels[index];
            currentModel.SetActive(true);
            cameraController.Focus(currentModel);
        }
        else
        {
            Debug.LogError("ImportController: SwitchModel index out of range " + index, this);
            return;
        }

        CurrentModelUpdate();
    }

    public void SwitchTexture(int index)
    {
        if (index == -1)
        {
            currentTexture = null;
        }
        else if (0 <= index && index < allTextures.Count)
        {
            currentTexture = allTextures[index];
        }
        else
        {
            Debug.LogError("ImportController: SwitchTexture index out of range " + index, this);
            return;
        }

        CurrentTextureUpdate();
    }

    public void ChangeModelSettings(int albedo, int metallic, int normal, int height)
    {
        if (currentModel == null)
            return;

        Material material = currentModel.GetComponent<Renderer>().sharedMaterial;

        Texture GetTexture(int index)
        {
            if (index == -1)
                return null;
            else if (0 <= index && index < allTextures.Count)
                return allTextures[index];
            else
            {
                Debug.LogError("ImportController: GetTexture index out of range " + index, this);
                return null;
            }
        }

        wavefrontShader.UpdateMaterial(material, GetTexture(albedo), GetTexture(metallic), GetTexture(normal), GetTexture(height));
    }

    public void ChangeTextureSettings(int wrap, int filter)
    {
        if (currentTexture == null)
            return;

        currentTexture.wrapMode = (TextureWrapMode)wrap;
        currentTexture.filterMode = (FilterMode)filter;
    }
}
