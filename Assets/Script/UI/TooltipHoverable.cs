using UnityEngine;
using UnityEngine.EventSystems;

public class TooltipHoverable : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    [Multiline()]
    private string tooltipText;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (Tooltip.Singleton != null)
            Tooltip.Singleton.Show(tooltipText);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (Tooltip.Singleton != null)
            Tooltip.Singleton.Hide();
    }
}
