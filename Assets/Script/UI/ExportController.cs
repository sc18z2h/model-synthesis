using System;
using System.Collections.Generic;
using UnityEngine;

public class ExportController : MonoBehaviour
{
    public GameObject exportSection;
    public ChunkPool generatorPool;
    public List<GameObject> exportModels;
    public CameraController cameraController;
    public ChunkGenerator chunkGenerator;

    private void OnEnable()
    {
        for (int i = 0; i < exportModels.Count; ++i)
            Destroy(exportModels[i]);
        exportModels.Clear();
        exportModels.Capacity = generatorPool.chunkPool.Count;
        for (int i = 0; i < generatorPool.chunkPool.Count; ++i)
        {
            GameObject copy = Instantiate(generatorPool.chunkPool[i].gameObject, exportSection.transform);
            exportModels.Add(copy);
        }

        exportSection.SetActive(true);

        if (0 < generatorPool.chunkPool.Count)
            cameraController.Focus(chunkGenerator.chunkPool.GetBounds());
    }

    private void OnDisable()
    {
        if (exportSection != null)
            exportSection.SetActive(false);
    }

    public string ExportModel(string path)
    {
        List<Mesh> meshes = new List<Mesh>();
        List<Vector3> offsets = new List<Vector3>();
        for (int i = 0; i < exportModels.Count; ++i)
        {
            meshes.Add(exportModels[i].GetComponent<MeshFilter>().sharedMesh);
            offsets.Add(exportModels[i].transform.position);
        }

        try
        {
            WavefrontExporter.ExportModels(path, meshes, offsets);
            return UIController.RandomSuccessString();
        }
        catch(Exception exception)
        {
            return exception.Message;
        }
    }
}
