using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour
{    
    public GameObject[] tabs;
    public CameraController cameraController;

    public static string RandomSuccessString() =>
        string.Format("<color=#{0}>Success!</color>", ColorUtility.ToHtmlStringRGB(UnityEngine.Random.ColorHSV()));

    private void Awake()
    {
        importController.OnModelNamesUpdate += OnModelNamesUpdate;
        importController.OnTextureNamesUpdate += OnTextureNamesUpdate;
        importController.OnCurrentModelUpdate += OnCurrentModelUpdate;
        importController.OnCurrentTextureUpdate += OnCurrentTextureUpdate;
    }

    private void Start()
    {
        SwitchTab(0);
        SelectSliceModel(0);
    }

    private float ParseFloat(TMP_InputField inputField, float min = float.MinValue, float max = float.MaxValue)
    {
        // [min, max] are both inclusive
        if (float.TryParse(inputField.text, out float number))
        {
            if (min <= number && number <= max)
                return number;
            else
            {
                number = Mathf.Clamp(number, min, max);
                inputField.text = number.ToString();
                return number;
            }
        }
        else
        {
            float defaultValue = Mathf.Clamp(default, min, max);
            inputField.text = defaultValue.ToString();
            return defaultValue;
        }
    }
    
    private int ParseInt(TMP_InputField inputField, int min = int.MinValue, int max = int.MaxValue)
    {
        // [min, max] are both inclusive
        if (int.TryParse(inputField.text, out int number))
        {
            if(min <= number && number <= max)
                return number;
            else
            {
                number = Mathf.Clamp(number, min, max);
                inputField.text = number.ToString();
                return number;
            }
        }
        else
        {
            int defaultValue = Mathf.Clamp(default, min, max);
            inputField.text = defaultValue.ToString();
            return defaultValue;
        }
    }

    public void SwitchTab(int index)
    {
        for (int i = 0; i < tabs.Length; ++i)
        {
            if (tabs[i].activeSelf)
            {
                cameraController.SaveOrientation(i);
                tabs[i].SetActive(false);
            }
        }

        cameraController.LoadOrientation(index);
        if (0 <= index && index < tabs.Length)
            tabs[index].SetActive(true);
        else
            Debug.LogError("UIController: SwitchTab failed, index out of range " + index);
    }

    public void ApplicationQuit()
    {
        Application.Quit();
    }

    [Header("Import")]
    public ImportController importController;

    public TMP_InputField modelPath;
    public TMP_InputField modelScale;
    public Toggle modelFlipUV;
    
    public void ImportModel()
    {
        importErrorMessage.text = importController.ImportModel(modelPath.text, ParseFloat(modelScale), modelFlipUV.isOn);
    }

    public TMP_InputField texturePath;

    public void ImportTexture()
    {
        importErrorMessage.text = importController.ImportTexture(texturePath.text);
    }

    public TMP_InputField importErrorMessage;

    public TMP_Dropdown selectedModelSettings;

    public void SelectedModelSettings(int index)
    {
        // input index is 1-based
        importController.SwitchModel(index - 1);
    }

    public TMP_Dropdown albedo, metallic, normal, height;

    public void ChangeModelSettings()
    {
        // index is 1-based
        importController.ChangeModelSettings(
            albedo.value - 1, 
            metallic.value - 1, 
            normal.value - 1, 
            height.value - 1);
    }

    public TMP_Dropdown selectedTextureSettings;

    public void SelectedTextureSettings(int index)
    {
        // input index is 1-based
        importController.SwitchTexture(index - 1);
    }

    public TMP_Dropdown wrapMode, filterMode;

    public void ChangeTextureSettings()
    {
        // index is 0-based
        importController.ChangeTextureSettings(wrapMode.value, filterMode.value);
    }

    private void OnModelNamesUpdate(List<string> names)
    {
        sliceModel.ClearOptions();
        sliceModel.AddOptions(names);
        names.Insert(0, "none");
        selectedModelSettings.ClearOptions();        
        selectedModelSettings.AddOptions(names);
    }

    private void OnTextureNamesUpdate(List<string> names)
    {
        IEnumerator<TMP_Dropdown> AllTextureDropdowns()
        {
            yield return selectedTextureSettings;
            yield return albedo;
            yield return metallic;
            yield return normal;
            yield return height;
            yield return maskTexture;
        }

        names.Insert(0, "none");
        IEnumerator<TMP_Dropdown> allTextureDropdowns = AllTextureDropdowns();
        while(allTextureDropdowns.MoveNext())
        {
            allTextureDropdowns.Current.ClearOptions();
            allTextureDropdowns.Current.AddOptions(names);            
        }
    }

    private void OnCurrentModelUpdate(int index, int albedo, int metallic, int normal, int height)
    {
        // input indices are 0-based
        selectedModelSettings.value = index + 1;
        this.albedo.value = albedo + 1;
        this.metallic.value = metallic + 1;
        this.normal.value = normal + 1;
        this.height.value = height + 1;
    }

    private void OnCurrentTextureUpdate(int index, int wrap, int filter)
    {
        // input indices are 0-based
        selectedTextureSettings.value = index + 1;
        wrapMode.value = wrap;
        filterMode.value = filter;
    }

    [Header("Slice")]
    public SliceController sliceController;
    public TMP_Dropdown sliceModel;

    public void SelectSliceModel(int index)
    {
        // index is 0-based
        if (0 <= index && index < importController.allModels.Count)
        {
            sliceController.SelectSliceModel(importController.allModels[index]);
            chunkGenerator.chunkPool.ChangeMaterials(importController.allModels[index].GetComponent<Renderer>());
            slicesDisplay.SetText(sliceController.VoxelCounts.ToString());
        }
        else
            sliceController.UndoSlice();        
    }

    public TMP_InputField offsetX, offsetY, offsetZ;
    public TextMeshProUGUI slicesDisplay;

    public void UpdateOffset()
    {
        sliceController.SetOffset(new Vector3(ParseFloat(offsetX), ParseFloat(offsetY), ParseFloat(offsetZ)));        
    }

    public TMP_InputField gridStepX, gridStepY, gridStepZ;

    public void UpdateGridStep()
    {
        sliceController.SetGridStep(new Vector3(ParseFloat(gridStepX), ParseFloat(gridStepY), ParseFloat(gridStepZ)));        
    }

    public void Slice()
    {
        sliceController.Slice();
        slicesDisplay.SetText(sliceController.VoxelCounts.ToString());
    }

    public void UndoSlice()
    {
        sliceController.UndoSlice();
    }

    [Header("Generate")]
    public ChunkGenerator chunkGenerator;    
    public TMP_InputField
        outputDimensionX, outputDimensionY, outputDimensionZ,
        backtrackProbability,
        backtrackSteps,
        maxIterations,

        seeds,
        maxSeedAttempts,
        maxRestarts;

    public TMP_Dropdown
        maskTexture,
        maskAxes;
    public DropdownFlags groundedDirections;
    public TMP_InputField
        generateErrorMessage;

    public void GenerateInParallel()
    {
        UpdateChunkGenerator();
        string errorMessage = chunkGenerator.GenerateParallel();
        if (errorMessage.Length == 0)
            errorMessage = RandomSuccessString();
        generateErrorMessage.text = errorMessage;
        FocusOnGenerated();
    }

    public void Generate()
    {
        UpdateChunkGenerator();
        string errorMessage = chunkGenerator.Generate();
        if (errorMessage.Length == 0)
            errorMessage = RandomSuccessString();
        generateErrorMessage.text = errorMessage;
        FocusOnGenerated();
    }

    private void UpdateChunkGenerator()
    {
        chunkGenerator.outputDimensions = 
            new Vector3Int(ParseInt(outputDimensionX, 1), ParseInt(outputDimensionY, 1), ParseInt(outputDimensionZ, 1));

        chunkGenerator.backtrackProbability = ParseFloat(backtrackProbability, 0, 1);
        chunkGenerator.backtrackAmount = ParseInt(backtrackSteps, 0);
        chunkGenerator.maxIterations = ParseInt(maxIterations, 0);

        chunkGenerator.seeds = ParseInt(seeds, 0);
        chunkGenerator.maxSeedAttempts = ParseInt(maxSeedAttempts, 0);
        chunkGenerator.maxRestarts = ParseInt(maxRestarts, 0);

        // index is 1-based
        int textureIndex = maskTexture.value - 1;
        if (0 <= textureIndex && textureIndex < importController.allTextures.Count)
            chunkGenerator.maskTexture = importController.allTextures[textureIndex] as Texture2D;
        else
            chunkGenerator.maskTexture = null;
        chunkGenerator.maskAxes = (ChunkGenerator.MaskAxes)maskAxes.value;
        chunkGenerator.groundedDirections = (OrthogonalDirection.MultiDirection)groundedDirections.GetFlags();

        chunkGenerator.chunkPool.gridStep = sliceController.chunkSlicer.gridStep;
    }

    private void FocusOnGenerated()
    {
        if (chunkGenerator.chunkPool.chunkPool.Count == 0)
            return;
        cameraController.Focus(chunkGenerator.chunkPool.GetBounds());
    }

    public TMP_InputField
        cameraSpeed,
        cameraAngle,
        cameraHeight,
        cameraForward;

    public void StartDemo()
    {
        UpdateChunkGenerator();
        UpdateDemoCameraSettings();
        chunkGenerator.StartInfiniteGenerationDemo();
    }

    public void StopDemo()
    {
        chunkGenerator.StopInfiniteGenerationDemo();
    }

    public void UpdateDemoCameraSettings()
    {
        chunkGenerator.cameraSpeed = ParseFloat(cameraSpeed, 0);
        chunkGenerator.cameraAngle = ParseFloat(cameraAngle, 0, 180);
        chunkGenerator.cameraHeight = ParseFloat(cameraHeight);
        chunkGenerator.cameraForward = ParseFloat(cameraForward);
    }

    [Header("Export")]
    public ExportController exportController;
    public TMP_InputField exportPath, exportErrorMessage;

    public void ExportModel()
    {
        exportErrorMessage.text = exportController.ExportModel(exportPath.text);
    }
}
