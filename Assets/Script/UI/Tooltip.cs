using UnityEngine;
using TMPro;

public class Tooltip : MonoBehaviour
{
    public static Tooltip Singleton { get; private set; }

    [SerializeField]
    private TextMeshProUGUI label;
    [SerializeField]
    private RectTransform labelBackground;
    [SerializeField]
    private float fixedWidth = 400;

    private void Awake()
    {
        if (Singleton == null)
            Singleton = this;
        else
        {            
            Debug.LogWarning("Tooltip singleton already exists, ensure there is only ever 1.");
            Destroy(gameObject);
            return;
        }
        Hide();
    }

    private void Update()
    {
        transform.position = Input.mousePosition;
    }

    [ContextMenu("Test")]
    private void Test()
    {        
        Show(new string('x', Random.Range(5, 50)));
    }

    public void Show(string text)
    {
        label.SetText(text);
        Vector2 size = new Vector2(Mathf.Min(fixedWidth, label.preferredWidth) + 10, label.preferredHeight + 10);        
        labelBackground.sizeDelta = size;
        Update();
        gameObject.SetActive(true);        
    }

    public void Hide()
    {
        label.SetText(string.Empty);
        gameObject.SetActive(false);
    }
}
