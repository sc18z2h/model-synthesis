using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DropdownFlags : MonoBehaviour
{
    public GameObject dropdownMenu;
    public Toggle none;
    public Toggle everything;
    public Toggle[] toggleFlags;
    public TextMeshProUGUI label;

    private void Start()
    {
        if (none != null)
        {
            none.onValueChanged.RemoveAllListeners();
            none.onValueChanged.AddListener((b) => OnNoneClicked());
            none.SetIsOnWithoutNotify(true);
        }

        if (everything != null)
        {
            everything.onValueChanged.RemoveAllListeners();
            everything.onValueChanged.AddListener((b) => OnEverythingClicked());
            everything.SetIsOnWithoutNotify(false);
        }

        for (int i = 0; i < toggleFlags.Length; ++i)
        {
            if (toggleFlags[i] == null)
                continue;

            toggleFlags[i].onValueChanged.RemoveAllListeners();
            toggleFlags[i].onValueChanged.AddListener((b) => OnFlagClicked());
            toggleFlags[i].SetIsOnWithoutNotify(false);
        }

        MenuUpdate(dropdownMenu.activeSelf);
    }

    public void OnClicked()
    {
        MenuUpdate(!dropdownMenu.activeSelf);
    }

    public void OnNoneClicked()
    {
        none.SetIsOnWithoutNotify(true);
        everything.SetIsOnWithoutNotify(false);
        foreach(var toggle in toggleFlags)
            toggle.SetIsOnWithoutNotify(false);

        MenuUpdate(false);
    }

    public void OnEverythingClicked()
    {        
        none.SetIsOnWithoutNotify(false);
        everything.SetIsOnWithoutNotify(true);
        foreach (var toggle in toggleFlags)
            toggle.SetIsOnWithoutNotify(true);

        MenuUpdate(false);
    }

    public void OnFlagClicked()
    {
        int isOnCount = MenuUpdate(false);
        
        if (isOnCount == 0)
        {
            none.SetIsOnWithoutNotify(true);
            everything.SetIsOnWithoutNotify(false);
        }
        else if (isOnCount == toggleFlags.Length)
        {
            none.SetIsOnWithoutNotify(false);
            everything.SetIsOnWithoutNotify(true);
        }
        else
        {
            none.SetIsOnWithoutNotify(false);
            everything.SetIsOnWithoutNotify(false);
        }
    }

    public int GetFlags()
    {
        if (none.isOn)
            return 0;
        else if (everything.isOn)
            return ~0;

        int flags = 0;
        for (int i = 0; i < toggleFlags.Length; ++i)
            if (toggleFlags[i].isOn)
                flags |= 1 << i;

        return flags;
    }

    private int MenuUpdate(bool setDropdown)
    {
        dropdownMenu.SetActive(setDropdown);

        int lastIsOnIndex = -1;
        int isOnCount = 0;
        for (int i = 0; i < toggleFlags.Length; ++i)
            if (toggleFlags[i].isOn)
            {
                lastIsOnIndex = i;
                ++isOnCount;
            }

        if (isOnCount == 0)
            label.SetText("none");
        else if (isOnCount == toggleFlags.Length)
            label.SetText("everything");
        else if (isOnCount == 1)
            label.SetText(toggleFlags[lastIsOnIndex].GetComponentInChildren<TextMeshProUGUI>().text);
        else
            label.SetText("mixed ...");

        return isOnCount;
    }
}
