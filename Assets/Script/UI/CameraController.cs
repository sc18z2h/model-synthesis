using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Camera myCamera;    
    public float movementSpeed = 0.5f;
    public float rotationSpeed = 150.0f;
    public float movementAcceleration = 1.0f;
    public float scrollSpeed = 1.0f;

    public Vector3[] savedPositions;
    public Quaternion[] savedRotations;
    public float movementSpeedScale = 13f;

    private float rotationX, rotationY;
    private float currentSpeed;
    private float currentScrollSpeed;

    private void Awake()
    {
        for(int i = 0; i < savedPositions.Length; ++i)
        {
            savedPositions[i] = transform.position;
            savedRotations[i] = transform.rotation;
        }            
    }

    private void Start()
    {
        CalibrateRotation();
        currentSpeed = movementSpeed;
        currentScrollSpeed = 0;
    }

    private void Update()
    {
        const string
            horizontal = "Horizontal",
            vertical = "Vertical",
            mouseX = "Mouse X",
            mouseY = "Mouse Y",
            rightMouseButton = "Fire2",
            mouseWheel = "Mouse ScrollWheel";

        if (Input.GetButton(rightMouseButton))
        {
            rotationX = Mathf.Clamp(rotationX - Input.GetAxis(mouseY) * rotationSpeed * Time.deltaTime, -90, 90);
            rotationY += Input.GetAxis(mouseX) * rotationSpeed * Time.deltaTime;

            transform.rotation = Quaternion.Euler(rotationX, rotationY, 0);
        }

        float moveUp = 0.0f;
        if (Input.GetKey(KeyCode.E))
            moveUp += 1.0f;
        if(Input.GetKey(KeyCode.Q))
            moveUp -= 1.0f;
        
        Vector3 translate = new Vector3(Input.GetAxis(horizontal), moveUp, Input.GetAxis(vertical));
        if (translate == Vector3.zero)
        {
            currentSpeed = movementSpeed;
            currentScrollSpeed = 0;
        }
        else
        {
            if (1 < translate.sqrMagnitude)
                translate.Normalize();

            currentScrollSpeed = Mathf.Clamp(currentScrollSpeed + Input.GetAxis(mouseWheel), -1, 1);
            float scrollModifier = Mathf.Pow(2, currentScrollSpeed);

            transform.Translate(translate * currentSpeed * scrollModifier * Time.deltaTime, Space.Self);
            currentSpeed += movementAcceleration * scrollModifier * Time.deltaTime;
        }
    }

    public void Focus(GameObject gameObject)
    {
        Focus(gameObject.GetComponent<MeshFilter>().sharedMesh.bounds);
    }

    public void Focus(Bounds bounds)
    {
        Vector3 vectorTo = bounds.center - transform.position;
        if (vectorTo == Vector3.zero)
            vectorTo = Vector3.forward;
        else
            vectorTo.Normalize();

        /*
        float maxExtent = Mathf.Max(bounds.extents.x, Mathf.Max(bounds.extents.y, bounds.extents.z));
        float idealDistance = maxExtent / Mathf.Tan(myCamera.fieldOfView / 2.0f * Mathf.Deg2Rad);
        transform.position = bounds.center - vectorTo * idealDistance * 2.0f;
        */

        transform.rotation = Quaternion.LookRotation(vectorTo, Vector3.up);
        CalibrateRotation();

        Vector3 size = bounds.size;
        movementSpeed = (size.x + size.y + size.z) / 3.0f / movementSpeedScale;
    }

    public void SaveOrientation(int index)
    {
        if (0 <= index && index < savedPositions.Length)
        {
            savedPositions[index] = transform.position;
            savedRotations[index] = transform.rotation;
        }
        else
            Debug.LogError("CameraController: SaveOrientation failed, index out of range " + index);        
    }

    public void LoadOrientation(int index)
    {
        if (0 <= index && index < savedPositions.Length)
        {
            transform.position = savedPositions[index];
            transform.rotation = savedRotations[index];
            CalibrateRotation();
        }
        else
            Debug.LogError("CameraController: LoadOrientation failed, index out of range " + index);
    }

    private void CalibrateRotation()
    {
        Vector3 euler = transform.rotation.eulerAngles;
        rotationX = 180 < euler.x ? euler.x - 360 : euler.x;
        rotationY = euler.y;
    }
}
