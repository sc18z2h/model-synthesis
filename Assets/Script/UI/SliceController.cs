using System.Collections.Generic;
using UnityEngine;

public class SliceController : MonoBehaviour
{
    public GameObject sliceSection;
    public ChunkSlicer chunkSlicer;
    public ChunkPool chunkPool;
    public GameObject slicePlanePrefab;    
    public GameObject sliceModel;
    public Vector3Int VoxelCounts { get; private set; }

    private bool isSliced;
    [SerializeField]
    private List<GameObject> slicePlanes;
    private GameObject selectedModel;
    private Vector3 offset;
    [SerializeField]
    private CameraController cameraController;

    private void OnEnable()
    {
        sliceSection.SetActive(true);
        if(selectedModel != null)
            cameraController.Focus(selectedModel);
    }

    private void OnDisable()
    {
        if (sliceSection != null)
        {
            sliceSection.SetActive(false);
            if (isSliced == false)
            {
                Slice();
                UndoSlice();
            }
        }
    }

    public void UndoSlice()
    {
        SelectSliceModel(selectedModel);
    }

    public void SelectSliceModel(GameObject sliceModel)
    {
        isSliced = false;
        chunkPool.ResetPool();

        if (sliceModel == null)
        {
            this.sliceModel.SetActive(false);
        }
        else
        {
            if (selectedModel != sliceModel)
            {
                this.sliceModel.GetComponent<MeshFilter>().sharedMesh =
                    sliceModel.GetComponent<MeshFilter>().sharedMesh;
                this.sliceModel.GetComponent<Renderer>().sharedMaterials =
                    sliceModel.GetComponent<Renderer>().sharedMaterials;
            }
            this.sliceModel.SetActive(true);
            cameraController.Focus(sliceModel);
        }
        selectedModel = sliceModel;
        UpdateSlicePlanes();
    }

    public void SetOffset(Vector3 offset)
    {
        this.offset = offset;
        UndoSlice();
    }

    public void SetGridStep(Vector3 gridStep)
    {
        if (gridStep.x == 0)
            gridStep.x = 0.1f;
        if (gridStep.y == 0)
            gridStep.y = 0.1f;
        if (gridStep.z == 0)
            gridStep.z = 0.1f;

        chunkSlicer.gridStep = gridStep;
        UndoSlice();
    }

    public void Slice()
    {
        if (selectedModel != null)
        {
            isSliced = true;
            chunkSlicer.offset = offset;
            chunkSlicer.spacing = chunkSlicer.gridStep * 0.1f;
            chunkSlicer.SliceVoxels();
            sliceModel.SetActive(false);
            UpdateSlicePlanes();
        }
    }

    private void UpdateSlicePlanes()
    {
        if (isSliced || sliceModel.activeSelf == false)
        {
            for (int i = 0; i < slicePlanes.Count; ++i)
                Destroy(slicePlanes[i]);
            slicePlanes.Clear();
        }
        else
        {
            Mesh copyMesh = Instantiate(sliceModel.GetComponent<MeshFilter>().sharedMesh);
            Vector3[] vertices = copyMesh.vertices;
            chunkSlicer.FindBounds(vertices,
                out Vector3 lowerBound, out Vector3 upperBound, out Vector3Int voxelCounts);
            VoxelCounts = voxelCounts;
            Vector3 boundingBox = upperBound - lowerBound;

            int slicePlanesIndex = 0;
            void DrawPlane(Vector3 position, Quaternion rotation, Vector3 scale)
            {
                GameObject slicePlane;
                if (slicePlanesIndex < slicePlanes.Count)
                {
                    slicePlane = slicePlanes[slicePlanesIndex];
                }
                else
                {
                    slicePlane = Instantiate(slicePlanePrefab, sliceSection.transform);
                    slicePlane.SetActive(true);
                    slicePlanes.Add(slicePlane);
                }
                Transform planeTransform = slicePlane.transform;
                planeTransform.position = position + offset;
                planeTransform.rotation = rotation;
                planeTransform.localScale = scale;
                ++slicePlanesIndex;
            }

            Quaternion planeRotation = Quaternion.identity;
            Vector3 planeScale = new Vector3(boundingBox.x, boundingBox.y, 1);
            for (int z = 1; z < voxelCounts.z; ++z)
            {
                DrawPlane(
                    lowerBound + new Vector3(0, 0, z * chunkSlicer.gridStep.z),
                    planeRotation, planeScale);
            }
            planeRotation = Quaternion.Euler(90, 0, 0);
            planeScale = new Vector3(boundingBox.x, boundingBox.z, 1);
            for (int y = 1; y < voxelCounts.y; ++y)
            {
                DrawPlane(
                    lowerBound + new Vector3(0, y * chunkSlicer.gridStep.y, 0),
                    planeRotation, planeScale);
            }
            planeRotation = Quaternion.Euler(0, -90, 0);
            planeScale = new Vector3(boundingBox.z, boundingBox.y, 1);
            for (int x = 1; x < voxelCounts.x; ++x)
            {
                DrawPlane(
                    lowerBound + new Vector3(x * chunkSlicer.gridStep.x, 0, 0),
                    planeRotation, planeScale);
            }

            int removeStart = slicePlanesIndex;
            for (; slicePlanesIndex < slicePlanes.Count; ++slicePlanesIndex)
                Destroy(slicePlanes[slicePlanesIndex]);
            if (removeStart < slicePlanes.Count)
                slicePlanes.RemoveRange(removeStart, slicePlanes.Count - removeStart);
        }
    }
}
