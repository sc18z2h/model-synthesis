using UnityEngine;
using System;
[Serializable]
public struct PositionNormalPair : IComparable<PositionNormalPair>
{
    public Vector3 position;
    public Vector3 normal;
    public PositionNormalPair(Vector3 position, Vector3 normal)
    {
        this.position = position;
        this.normal = normal;
    }
    public static bool operator ==(PositionNormalPair x, PositionNormalPair y)
    {
        return x.position == y.position && x.normal == y.normal;
    }
    public static bool operator !=(PositionNormalPair x, PositionNormalPair y)
    {
        return x.position != y.position || x.normal != y.normal;
    }
    public override bool Equals(object obj)
    {
        if (obj != null && obj is PositionNormalPair)
            return this == (PositionNormalPair)obj;
        else
            return false;
    }
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
    /// <summary>
    ///    Less than 0:     this is less than y.
    ///              0:     this equals y.
    /// Greater than 0: 	this is greater than y.
    /// </summary>
    public int CompareTo(PositionNormalPair other)
    {
        if (Mathf.Approximately(position.x, other.position.x) == false)
            return position.x.CompareTo(other.position.x);
        else if (Mathf.Approximately(position.y, other.position.y) == false)
            return position.y.CompareTo(other.position.y);
        else if (Mathf.Approximately(position.z, other.position.z) == false)
            return position.z.CompareTo(other.position.z);

        else if (Mathf.Approximately(normal.x, other.normal.x) == false)
            return normal.x.CompareTo(other.normal.x);
        else if (Mathf.Approximately(normal.y, other.normal.y) == false)
            return normal.y.CompareTo(other.normal.y);
        else if (Mathf.Approximately(normal.z, other.normal.z) == false)
            return normal.z.CompareTo(other.normal.z);

        else
            return 0;
    }
}