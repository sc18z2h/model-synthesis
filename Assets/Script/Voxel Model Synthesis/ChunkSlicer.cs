﻿using System.Collections.Generic;
using UnityEngine;
using static OrthogonalDirection;

public class ChunkSlicer : MonoBehaviour
{
    [Header("References")]
    public ChunkPool chunkPool;
    public ChunkGenerator chunkGenerator;    

    [Header("Slice Settings")]
    public MeshFilter target;

    [Header("Grid Settings")]
    public Vector3 offset;
    public Vector3 gridStep = Vector3.one;
    public Vector3 spacing = Vector3.one * 0.1f;

    [ContextMenu("Slice Voxels")]
    public void SliceVoxels()
    {
        if (target != null)
        {
            // copy the shared mesh
            Mesh targetMesh;            
            {
                Mesh sharedMesh = target.sharedMesh;
                targetMesh = Instantiate(sharedMesh);
                targetMesh.name = sharedMesh.name;
            }

            Vector3Int voxelCounts;
            {
                Vector3[] vertices = targetMesh.vertices;
                FindBounds(vertices, out Vector3 lowerBound, out _, out voxelCounts);
                Vector3 correctionVector = -offset - lowerBound;
                // offset all vertices
                for (int index = 0; index < vertices.Length; ++index)
                    vertices[index] += correctionVector;
                targetMesh.vertices = vertices;
            }

            MeshChunk[,,] chunks = new MeshChunk[voxelCounts.z, voxelCounts.y, voxelCounts.x];            
            for (int z = 0; z < chunks.GetLength(0); ++z)
                for (int y = 0; y < chunks.GetLength(1); ++y)
                    for (int x = 0; x < chunks.GetLength(2); ++x)
                        chunks[z, y, x].Initialise();
            chunks[0, 0, 0] = new MeshChunk(targetMesh);

            List<PositionNormalPair>
                edgeVerticesZ = new List<PositionNormalPair>(),
                edgeVerticesY = new List<PositionNormalPair>(),
                edgeVerticesX = new List<PositionNormalPair>(),
                buffer = new List<PositionNormalPair>();
            void addToEdgeVerticesZ(Vector3 position, Vector3 normal, Vector2 uv, int triangle)
            {
                edgeVerticesZ.Add(new PositionNormalPair(position, normal));
            }
            void addToEdgeVerticesY(Vector3 position, Vector3 normal, Vector2 uv, int triangle)
            {
                edgeVerticesY.Add(new PositionNormalPair(position, normal));
            }
            void addToEdgeVerticesX(Vector3 position, Vector3 normal, Vector2 uv, int triangle)
            {
                edgeVerticesX.Add(new PositionNormalPair(position, normal));
            }
            bool cutZ, cutY;

            chunkGenerator.chunkConnections.Clear();

            for (int z = 0; z < chunks.GetLength(0); ++z)
            {
                if (chunks[z, 0, 0].mesh == null)
                    break;
                if ((cutZ = z < chunks.GetLength(0) - 1))
                {
                    edgeVerticesZ.Clear();
                    MeshSlicer.Slice(
                        chunks[z, 0, 0].mesh,
                        new HalfPlane(Vector3.forward, gridStep.z),
                        addToEdgeVerticesZ,
                        out chunks[z, 0, 0].mesh, out chunks[z + 1, 0, 0].mesh,
                        new Vector3(), new Vector3(0, 0, -gridStep.z));
                }

                for (int y = 0; y < chunks.GetLength(1); ++y)
                {
                    if (chunks[z, y, 0].mesh == null)
                        break;
                    if ((cutY = y < chunks.GetLength(1) - 1))
                    {
                        edgeVerticesY.Clear();
                        MeshSlicer.Slice(
                            chunks[z, y, 0].mesh,
                            new HalfPlane(Vector3.up, gridStep.y),
                            addToEdgeVerticesY,
                            out chunks[z, y, 0].mesh, out chunks[z, y + 1, 0].mesh,
                            new Vector3(), new Vector3(0, -gridStep.y, 0));
                    }

                    for (int x = 0; x < chunks.GetLength(2); ++x)
                    {
                        if (chunks[z, y, x].mesh == null)
                            break;
                        Vector3 upperLimit = Vector3.Scale(gridStep, new Vector3(x + 1, y + 1, z + 1));
                        if (cutZ)
                        {
                            FilterEdgeVertices(new Vector3Int(0, 0, 1), edgeVerticesZ, buffer, upperLimit, new Vector3(-gridStep.x * x, -gridStep.y * y, 0));
                            MeshChunk.AddConnection(Axis.z, FindConnectionIndex(buffer), ref chunks[z, y, x], ref chunks[z + 1, y, x]);
                        }
                        if (cutY)
                        {
                            FilterEdgeVertices(Vector3Int.up, edgeVerticesY, buffer, upperLimit, new Vector3(-gridStep.x * x, 0, 0));
                            MeshChunk.AddConnection(Axis.y, FindConnectionIndex(buffer), ref chunks[z, y, x], ref chunks[z, y + 1, x]);
                        }
                        if (x < chunks.GetLength(2) - 1) // cutX
                        {
                            edgeVerticesX.Clear();
                            MeshSlicer.Slice(
                                chunks[z, y, x].mesh,
                                new HalfPlane(Vector3.right, gridStep.x),
                                addToEdgeVerticesX,
                                out chunks[z, y, x].mesh, out chunks[z, y, x + 1].mesh,
                                new Vector3(), new Vector3(-gridStep.x, 0, 0));

                            FilterEdgeVertices(Vector3Int.right, edgeVerticesX, buffer, upperLimit, new Vector3());
                            MeshChunk.AddConnection(Axis.x, FindConnectionIndex(buffer), ref chunks[z, y, x], ref chunks[z, y, x + 1]);
                        }
                    }
                }
            }

            chunkGenerator.inputChunks.Clear();            
            int chunkIndex = 0;

            chunkPool.SetParameters(gridStep, spacing);
            chunkPool.ResetPool();
            chunkPool.ChangeMaterials(target.GetComponent<Renderer>());
            for (int z = 0; z < chunks.GetLength(0); ++z)
                for (int y = 0; y < chunks.GetLength(1); ++y)
                    for (int x = 0; x < chunks.GetLength(2); ++x)
                    {
                        if (chunks[z, y, x].mesh == null)
                            continue;

                        if (z == 0)
                            chunks[z, y, x].groundedDirections |= MultiDirection.negativeZ;
                        else if (z == chunks.GetLength(0) - 1)
                            chunks[z, y, x].groundedDirections |= MultiDirection.positiveZ;

                        if (y == 0)
                            chunks[z, y, x].groundedDirections |= MultiDirection.negativeY;
                        else if (y == chunks.GetLength(1) - 1)
                            chunks[z, y, x].groundedDirections |= MultiDirection.positiveY;

                        if (x == 0)
                            chunks[z, y, x].groundedDirections |= MultiDirection.negativeX;
                        else if (x == chunks.GetLength(2) - 1)
                            chunks[z, y, x].groundedDirections |= MultiDirection.positiveX;

                        chunkPool.Append(new Vector3Int(x, y, z), chunks[z, y, x].mesh, true, true);

                        // only add chunk if its unique.
                        List<MeshChunk> inputChunks = chunkGenerator.inputChunks;
                        bool found = false;
                        for (int i = 0; i < inputChunks.Count; ++i)
                        {
                            if (inputChunks[i] == chunks[z, y, x])
                            {
                                found = true;
                                break;
                            }
                        }
                        if (found == false)
                        {
                            chunkGenerator.inputChunks.Add(chunks[z, y, x]);
                            chunks[z, y, x].LinkToConnectionsList(chunkGenerator.chunkConnections, chunkIndex);
                            ++chunkIndex;                            
                        }
                    }
            chunkPool.Finish();
        }
    }
    public void FindBounds(
        Vector3[] vertices,
        out Vector3 lowerBound,
        out Vector3 upperBound,
        out Vector3Int voxelCounts)
    {
        if (vertices.Length == 0)
            throw new System.Exception("Cannot find bounds of empty vertices.");

        Vector3 lowest = new Vector3(
            Mathf.Floor(vertices[0].x),
            Mathf.Floor(vertices[0].y),
            Mathf.Floor(vertices[0].z));
        Vector3 highest = lowest;

        for (int i = 1; i < vertices.Length; ++i)
        {
            if (vertices[i].x < lowest.x)
                lowest.x = vertices[i].x;
            else if (highest.x < vertices[i].x)
                highest.x = vertices[i].x;

            if (vertices[i].y < lowest.y)
                lowest.y = vertices[i].y;
            else if (highest.y < vertices[i].y)
                highest.y = vertices[i].y;

            if (vertices[i].z < lowest.z)
                lowest.z = vertices[i].z;
            else if (highest.z < vertices[i].z)
                highest.z = vertices[i].z;
        }

        lowerBound = new Vector3(
            RoundDownToMultiple(lowest.x, gridStep.x),
            RoundDownToMultiple(lowest.y, gridStep.y),
            RoundDownToMultiple(lowest.z, gridStep.z));
        upperBound = new Vector3(
            RoundUpToMultiple(highest.x, gridStep.x),
            RoundUpToMultiple(highest.y, gridStep.y),
            RoundUpToMultiple(highest.z, gridStep.z));
        voxelCounts = new Vector3Int(
            Mathf.RoundToInt((upperBound.x - lowerBound.x) / gridStep.x),
            Mathf.RoundToInt((upperBound.y - lowerBound.y) / gridStep.y),
            Mathf.RoundToInt((upperBound.z - lowerBound.z) / gridStep.z));
    }
    /// <returns>Lower bound on value that is a multiple</returns>
    private float RoundDownToMultiple(float value, float multiple)
    {
        return Mathf.Floor(value / multiple) * multiple;
    }
    private float RoundUpToMultiple(float value, float multiple)
    {
        return Mathf.Ceil(value / multiple) * multiple;
    }
    /// <summary>
    /// Removes from edgeVertices into outputBuffer vectors that are below the upperLimit
    /// (except in the direction of axisUnit)
    /// And sets output vector's axisUnit to 0
    /// </summary>
    private void FilterEdgeVertices(
        Vector3Int axisUnit,
        List<PositionNormalPair> edgeVertices, List<PositionNormalPair> outputBuffer,
        Vector3 upperLimit, Vector3 outputOffset)
    {
        // a bit mask to keep only the directions we care.
        Vector3 otherAxes = Vector3.one - axisUnit;

        outputBuffer.Clear();
        for (int i = edgeVertices.Count - 1; 0 <= i; --i)
        {
            if ((otherAxes.x == 0 || edgeVertices[i].position.x <= upperLimit.x) &&
                (otherAxes.y == 0 || edgeVertices[i].position.y <= upperLimit.y) &&
                (otherAxes.z == 0 || edgeVertices[i].position.z <= upperLimit.z))
            {
                PositionNormalPair pair = edgeVertices[i];
                pair.position = Vector3.Scale(pair.position, otherAxes) + outputOffset;
                outputBuffer.Add(pair);
                edgeVertices.RemoveAt(i);
            }
        }
    }
    private int FindConnectionIndex(List<PositionNormalPair> edgeVertices)
    {
        edgeVertices.Sort();
        if (edgeVertices.Count == 0)
            return -1;

        List<ChunkConnection> chunkConnections = chunkGenerator.chunkConnections;
        for (int i = 0; i < chunkConnections.Count; ++i)
        {
            PositionNormalPair[] currentEdgeVertices = chunkConnections[i].edgeVertices;
            if (currentEdgeVertices.Length != edgeVertices.Count)
                continue;

            bool isMatch = true;
            for (int j = 0; j < currentEdgeVertices.Length; ++j)
            {
                if (currentEdgeVertices[j] != edgeVertices[j])
                {
                    isMatch = false;
                    break;
                }
            }

            if (isMatch)
                return i;
        }

        chunkConnections.Add(new ChunkConnection(edgeVertices));
        return chunkConnections.Count - 1;
    }
}
