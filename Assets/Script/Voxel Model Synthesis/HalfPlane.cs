﻿using UnityEngine;

[System.Serializable]
/// <summary>
/// 3D half plane defined as : ax + by + cz = parameter, with normal (a, b, c)
/// </summary>
public struct HalfPlane
{
    public Vector3 normal;
    public float parameter;
    public HalfPlane(Vector3 normal, float distanceToOrigin)
    {
        this.normal = normal;

        // d = sqrt(distance^2 (a^2 + b^2 + c^2))
        // sometimes d = d * sqrt(a^2 + b^2 + c^2)
        // distanceToOrigin is not abs, so that it can be set negative to move half plane to negative side
        parameter = distanceToOrigin * normal.magnitude;
    }
    /// <summary>
    /// Half plane test.
    /// </summary>
    /// <returns>true if point in positive side, otherwise false</returns>
    public bool HalfPlaneTest(Vector3 point)
    {
        return parameter < Vector3.Dot(normal, point);
    }
}
