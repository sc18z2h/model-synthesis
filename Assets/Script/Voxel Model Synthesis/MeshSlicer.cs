﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class MeshSlicer
{
    public delegate void RecieveVertex(Vector3 position, Vector3 normal, Vector2 uv, int triangle);
    private class MeshBuilder
    {
        public List<int> triangles;
        private List<int> externalVertices;
        public MeshBuilder()
        {
            triangles = new List<int>();
            externalVertices = new List<int>();
        }
        public Mesh Build(
            Vector3[] originalVertices, Vector2[] originalUvs,
            List<Vertex> edgeVertices,
            Vector3 offset,
            string name)
        {
            // This mess below basically organises vertex positions close to how triangles are declared.
            // In other words, new edgeVertices/Uvs are intermingled with originalVertices/Uvs.
            // Instead of appending all new edgeVertices/Uvs at the end.
            Mesh mesh = new Mesh();
            mesh.name = name;
            int expectedLength = originalVertices.Length + externalVertices.Count;
            List<Vector3> vertices = new List<Vector3>(expectedLength);
            List<Vector2> uvs = new List<Vector2>(expectedLength);
            Dictionary<int, int> originalIndices = new Dictionary<int, int>(originalVertices.Length);

            for (int i = 0; i < triangles.Count; ++i)
            {
                int triangleIndex = triangles[i];
                if (originalIndices.TryGetValue(triangleIndex, out int storedIndex))
                {
                    triangles[i] = storedIndex;
                }
                else
                {
                    if (triangleIndex < 0)
                    {
                        // use added vertices, not original vertices
                        // convert negative external index back into positive form
                        int externalIndex = NegativeCompliment(triangleIndex);
                        vertices.Add(edgeVertices[externalVertices[externalIndex]].position + offset);
                        uvs.Add(edgeVertices[externalVertices[externalIndex]].uv);
                    }
                    else
                    {
                        vertices.Add(originalVertices[triangleIndex] + offset);
                        uvs.Add(originalUvs[triangleIndex]);
                    }
                    triangles[i] = vertices.Count - 1;
                    originalIndices.Add(triangleIndex, vertices.Count - 1);
                }
            }

            mesh.SetVertices(vertices);
            mesh.SetUVs(0, uvs);
            mesh.SetTriangles(triangles, 0);
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            return mesh;
        }
        /// <summary>
        /// Returns a negative triangle index which indicates an external vertex,
        /// actual value must be calculated at Build()
        /// </summary>
        public int StoreExternalVertex(int edgeIndex)
        {
            externalVertices.Add(edgeIndex);
            return NegativeCompliment(externalVertices.Count - 1);
        }
        /// <summary>
        /// Similar to 2's compliment. But -1=>0, -2=>1, -3=>2 ...
        /// </summary>
        public static int NegativeCompliment(int number)
        {
            return -number - 1;
        }
    }
    private struct Triangle
    {
        public Vertex vertex1, vertex2, vertex3;
        public Triangle(Vector3[] vertices, Vector3[] normals, Vector2[] uvs, int index1, int index2, int index3)
        {
            vertex1 = new Vertex(vertices, normals, uvs, index1);
            vertex2 = new Vertex(vertices, normals, uvs, index2);
            vertex3 = new Vertex(vertices, normals, uvs, index3);
        }
        public void AddToMeshBuilder(MeshBuilder meshBuilder)
        {
            meshBuilder.triangles.Add(vertex1.triangleIndex);
            meshBuilder.triangles.Add(vertex2.triangleIndex);
            meshBuilder.triangles.Add(vertex3.triangleIndex);
        }
    }
    private struct Vertex
    {
        public Vector3 position;
        public Vector3 normal;
        public Vector2 uv;
        public int triangleIndex;
        public Vertex(Vector3 position, Vector3 normal, Vector2 uv, int triangleIndex)
        {
            this.position = position;
            this.normal = normal;
            this.uv = uv;
            this.triangleIndex = triangleIndex;
        }
        public Vertex(Vector3[] vertices, Vector3[] normals, Vector2[] uvs, int triangleIndex)
        {
            position = vertices[triangleIndex];
            normal = normals[triangleIndex];
            uv = uvs[triangleIndex];
            this.triangleIndex = triangleIndex;
        }
    }
    /// <summary>
    /// Attempts to slice target mesh into twain. Fails when slice "misses" target.
    /// </summary>
    /// <param name="target">mesh to be sliced</param>
    /// <param name="halfPlane">represents the angle and location of slice</param>
    /// <param name="edgeVertices">populated with newly created vertices along the slice</param>
    /// <param name="negativeOffset">offset applied to negative vertices</param>
    /// <param name="positiveOffset">offset applied to positive vertices</param>
    /// <param name="negativeSide">set to null if empty</param>
    /// <param name="positiveSide">set to null if empty</param>
    /// <returns>true if slice succeeds, otherwise false</returns>
    public static bool Slice(
        Mesh target, HalfPlane halfPlane, RecieveVertex recieveEdgeVertices,
        out Mesh negativeSide, out Mesh positiveSide,
        Vector3 negativeOffset = new Vector3(), Vector3 positiveOffset = new Vector3())
    {
        Vector3[] vertices = target.vertices;
        Vector3[] normals = target.normals;
        Vector2[] uvs = target.uv;
        int[] triangles = target.triangles;

        List<Vertex> edgeVertexStructs = new List<Vertex>(); // list of vertex data structs of edge vertices
        List<int> slicedTriangles = new List<int>(); // list of original triangle indices of sliced triangles

        MeshBuilder
            positiveBuilder = new MeshBuilder(),
            negativeBuilder = new MeshBuilder();

        for (int i = 0; i < triangles.Length; i += 3)
        {
            Triangle triangle = new Triangle(vertices, normals, uvs,
                triangles[i], triangles[i + 1], triangles[i + 2]);

            bool test1 = halfPlane.HalfPlaneTest(triangle.vertex1.position);
            bool test2 = halfPlane.HalfPlaneTest(triangle.vertex2.position);
            bool test3 = halfPlane.HalfPlaneTest(triangle.vertex3.position);

            if (test1 == test2 && test2 == test3)
            {
                // all on the same side
                if (test1)
                    triangle.AddToMeshBuilder(positiveBuilder);
                else
                    triangle.AddToMeshBuilder(negativeBuilder);
            }
            else
            {
                if (test1 == test2)
                {
                    SliceTriangle(
                        triangle.vertex3, triangle.vertex1, triangle.vertex2,
                        halfPlane,
                        edgeVertexStructs,
                        slicedTriangles,
                        test3 ? positiveBuilder : negativeBuilder,
                        test3 ? negativeBuilder : positiveBuilder);
                }
                else if (test2 == test3)
                {
                    SliceTriangle(
                        triangle.vertex1, triangle.vertex2, triangle.vertex3,
                        halfPlane,
                        edgeVertexStructs,
                        slicedTriangles,
                        test1 ? positiveBuilder : negativeBuilder,
                        test1 ? negativeBuilder : positiveBuilder);
                }
                else // test1 == test3
                {
                    SliceTriangle(
                        triangle.vertex2, triangle.vertex3, triangle.vertex1,
                        halfPlane,
                        edgeVertexStructs,
                        slicedTriangles,
                        test2 ? positiveBuilder : negativeBuilder,
                        test2 ? negativeBuilder : positiveBuilder);
                }
            }
        }

        if (negativeBuilder.triangles.Count == 0)
        {
            negativeSide = null;
            positiveSide = positiveBuilder.Build(
                vertices, uvs,
                edgeVertexStructs,
                positiveOffset,
                target.name);
            return false;
        }
        else if (positiveBuilder.triangles.Count == 0)
        {
            negativeSide = negativeBuilder.Build(
                vertices, uvs,
                edgeVertexStructs,
                negativeOffset,
                target.name);
            positiveSide = null;
            return false;
        }
        else
        {
            negativeSide = negativeBuilder.Build(
                vertices, uvs,
                edgeVertexStructs,
                negativeOffset,
                target.name);
            positiveSide = positiveBuilder.Build(
                vertices, uvs,
                edgeVertexStructs,
                positiveOffset,
                target.name);
            if (recieveEdgeVertices != null)
                for (int i = 0; i < edgeVertexStructs.Count; ++i)
                    recieveEdgeVertices(
                        edgeVertexStructs[i].position,
                        edgeVertexStructs[i].normal,
                        edgeVertexStructs[i].uv,
                        edgeVertexStructs[i].triangleIndex);
            return true;
        }
    }
    /// <summary>
    /// Slices vertex1 away from vertex2 and vertex3. Adds new outlier triangle in outlierBuilder. And other triangles formed inside otherBuilder.
    /// Vertices must be inputed in clockwise order, aka cull order.
    /// </summary>
    private static void SliceTriangle(
        Vertex vertex1, Vertex vertex2, Vertex vertex3,
        HalfPlane halfPlane,
        List<Vertex> edgeVertexStructs,
        List<int> slicedTriangles,
        MeshBuilder outlierBuilder, MeshBuilder otherBuilder)
    {
        Vector3 vertex4Direction = vertex2.position - vertex1.position;
        Vector3 vertex5Direction = vertex3.position - vertex1.position;

        Vector3 vertex4 = LineIntersectHalfPlane(vertex1.position, vertex4Direction.normalized, halfPlane);
        Vector3 vertex5 = LineIntersectHalfPlane(vertex1.position, vertex5Direction.normalized, halfPlane);

        // we assume flat shading, so whole triangle has the same normal
        Vector3 normal = vertex1.normal;

        Vector2 uv4 = Vector2.Lerp(vertex1.uv, vertex2.uv, (vertex4 - vertex1.position).magnitude / vertex4Direction.magnitude);
        Vector2 uv5 = Vector2.Lerp(vertex1.uv, vertex3.uv, (vertex5 - vertex1.position).magnitude / vertex5Direction.magnitude);

        // try to simplify triangles, if another sliced triangle shares similar properties
        for (int i = 0; i < edgeVertexStructs.Count; ++i)
        {
            // shares at least 1 edge vertex
            byte sharedVertex = 0;
            if (edgeVertexStructs[i].position == vertex4) // == operator approximates
                sharedVertex = 4;
            else if (edgeVertexStructs[i].position == vertex5) // == operator approximates
                sharedVertex = 5;

            if (sharedVertex != 0)
            {
                // collinearity test (do edge points form 3D line?)
                int edgeVertexPartner = i % 2 == 0 ? i + 1 : i - 1;
                Vector3 edge1 = edgeVertexStructs[edgeVertexPartner].position - edgeVertexStructs[i].position;
                Vector3 edge2 = vertex5 - vertex4;
                if (Vector3.Cross(edge1, edge2) == Vector3.zero) // == operator approximates
                {
                    // test if this triangle (outlier, vertex2, vertex3) shares at least 2 vertices with other triangle
                    int slicedTriangle = i / 2 * 3;
                    if ((sharedVertex == 4 &&
                            (vertex1.triangleIndex == slicedTriangles[slicedTriangle + 1] &&
                             vertex2.triangleIndex == slicedTriangles[slicedTriangle]))
                        ||
                        (sharedVertex == 5 &&
                            (vertex1.triangleIndex == slicedTriangles[slicedTriangle + 2] &&
                             vertex3.triangleIndex == slicedTriangles[slicedTriangle])))
                    {
                        // now, we can simplify triangles                        
                        if (sharedVertex == 4)
                        {
                            // move common vertex to the outer vertex
                            // in certain cases, uv5 will not be correct
                            edgeVertexStructs[i] = new Vertex(vertex5, normal, uv5, edgeVertexStructs[i].triangleIndex);
                            // add 1 new triangle (4', 1', 3)
                            int vertex4PrimeIndex = i % 2 == 0 ? i : i - 1;
                            otherBuilder.triangles.Add(edgeVertexStructs[vertex4PrimeIndex].triangleIndex);
                            otherBuilder.triangles.Add(slicedTriangles[slicedTriangle]);
                            otherBuilder.triangles.Add(vertex3.triangleIndex);
                        }
                        else
                        {
                            // move common vertex to the outer vertex
                            // in certain cases, uv5 will not be correct
                            edgeVertexStructs[i] = new Vertex(vertex4, normal, uv4, edgeVertexStructs[i].triangleIndex);
                            // add 1 new triangle (1', 5', 2)
                            int vertex5PrimeIndex = i % 2 == 0 ? i + 1 : i;
                            otherBuilder.triangles.Add(slicedTriangles[slicedTriangle]);
                            otherBuilder.triangles.Add(edgeVertexStructs[vertex5PrimeIndex].triangleIndex);
                            otherBuilder.triangles.Add(vertex2.triangleIndex);
                        }
                        return;
                    }
                }
            }
        }

        int triangle4 = outlierBuilder.StoreExternalVertex(edgeVertexStructs.Count);
        int triangle5 = outlierBuilder.StoreExternalVertex(edgeVertexStructs.Count + 1);
        // these should be the same as triangle4 and triangle5
        otherBuilder.StoreExternalVertex(edgeVertexStructs.Count);
        otherBuilder.StoreExternalVertex(edgeVertexStructs.Count + 1);

        edgeVertexStructs.Add(new Vertex(vertex4, normal, uv4, triangle4));
        edgeVertexStructs.Add(new Vertex(vertex5, normal, uv5, triangle5));

        // triangle 1
        outlierBuilder.triangles.Add(vertex1.triangleIndex);
        outlierBuilder.triangles.Add(triangle4);
        outlierBuilder.triangles.Add(triangle5);

        // triangle 2
        otherBuilder.triangles.Add(triangle4);
        otherBuilder.triangles.Add(vertex2.triangleIndex);
        otherBuilder.triangles.Add(vertex3.triangleIndex);

        // triangle 3
        otherBuilder.triangles.Add(triangle4);
        otherBuilder.triangles.Add(vertex3.triangleIndex);
        otherBuilder.triangles.Add(triangle5);

        slicedTriangles.Add(vertex1.triangleIndex);
        slicedTriangles.Add(vertex2.triangleIndex);
        slicedTriangles.Add(vertex3.triangleIndex);
    }
    private static Vector3 LineIntersectHalfPlane(Vector3 lineStart, Vector3 lineDirection, HalfPlane halfPlane)
    {
        // parameterised line with parameter t:
        // t = (d - a(x0) - b(y0) - c(z0)) / (a * (x1 - x0) + b * (y1 - y0) + c * (z1 - z0))
        // t = (parameter - lineStart . normal) / (lineDirection . normal)
        float lineDotNormal = Vector3.Dot(lineDirection, halfPlane.normal);
        if (lineDotNormal == 0.0f)
        {
            // pretty abnormal behaviour
            return lineStart;
        }

        float t = (halfPlane.parameter - Vector3.Dot(lineStart, halfPlane.normal)) / lineDotNormal;
        return lineStart + t * lineDirection;
    }
}
