﻿using System.Collections.Generic;
[System.Serializable]
public struct ChunkConnection
{
    public PositionNormalPair[] edgeVertices;
    /// <summary>
    /// Stores the indices of compatible chunks.
    /// </summary>
    public List<int> chunks;
    public ChunkConnection(List<PositionNormalPair> edgeVertices)
    {
        this.edgeVertices = edgeVertices.ToArray();
        chunks = new List<int>();
    }
}
