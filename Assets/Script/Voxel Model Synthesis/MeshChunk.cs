﻿using System.Collections.Generic;
using UnityEngine;
using static OrthogonalDirection;

[System.Serializable]
public struct MeshChunk
{
    public Mesh mesh;

    // possible connections
    // note: these ints represent the index in the connections array (aka connectionId)
    public int right;
    public int left;
    public int up;
    public int down;
    public int forward;
    public int backward;

    public MultiDirection groundedDirections;

    public MeshChunk(Mesh mesh)
    {
        this.mesh = mesh;
        right = left = up = down = forward = backward = -1;
        groundedDirections = MultiDirection.none;
    }
    public void Initialise()
    {
        right = left = up = down = forward = backward = -1;
    }
    public static void AddConnection(Axis alongAxis, int connectionIndex, ref MeshChunk negativeDirection, ref MeshChunk positiveDirection)
    {
        switch (alongAxis)
        {
            case Axis.x:
                negativeDirection.right = connectionIndex;
                positiveDirection.left = connectionIndex;
                return;
            case Axis.y:
                negativeDirection.up = connectionIndex;
                positiveDirection.down = connectionIndex;
                return;
            case Axis.z:
                negativeDirection.forward = connectionIndex;
                positiveDirection.backward = connectionIndex;
                return;
            default:
                throw new System.Exception("AddConnection : invalid Axis enum");
        }
    }
    public int GetConnectionIndex(int direction)
    {
        switch (direction)
        {
            case 0:
                return right;
            case 1:
                return left;
            case 2:
                return up;
            case 3:
                return down;
            case 4:
                return forward;
            case 5:
                return backward;
            default:
                throw new System.Exception("GetPossibleDirection : unknown direction " + direction);
        }
    }
    public void LinkToConnectionsList(List<ChunkConnection> connections, int chunkIndex)
    {
        LinkDirection(right, connections, chunkIndex);
        LinkDirection(left, connections, chunkIndex);
        LinkDirection(up, connections, chunkIndex);
        LinkDirection(down, connections, chunkIndex);
        LinkDirection(forward, connections, chunkIndex);
        LinkDirection(backward, connections, chunkIndex);
    }
    private void LinkDirection(int direction, List<ChunkConnection> connections, int chunkIndex)
    {
        if (direction == -1)
            return;

        List<int> chunks = connections[direction].chunks;
        // check if linked already (if linked already, it would be the last item)
        if (0 < chunks.Count && chunks[chunks.Count - 1] == chunkIndex)
            return;

        chunks.Add(chunkIndex);
    }
    public bool CheckGrounded(
        MultiDirection forceGroundedDirections,
        bool isMinX, bool isMaxX, bool isMinY, bool isMaxY, bool isMinZ, bool isMaxZ)
    {
        MultiDirection combineFlags = forceGroundedDirections & groundedDirections;
        if (combineFlags == MultiDirection.none)
            return true;

        // if this has grounded directions, it can only lie on the extreme end of that direction
        if (combineFlags.HasFlag(MultiDirection.negativeX) && isMinX == false)
            return false;
        if (combineFlags.HasFlag(MultiDirection.positiveX) && isMaxX == false)
            return false;

        if (combineFlags.HasFlag(MultiDirection.negativeY) && isMinY == false)
            return false;
        if (combineFlags.HasFlag(MultiDirection.positiveY) && isMaxY == false)
            return false;

        if (combineFlags.HasFlag(MultiDirection.negativeZ) && isMinZ == false)
            return false;
        if (combineFlags.HasFlag(MultiDirection.positiveZ) && isMaxZ == false)
            return false;

        return true;
    }

    private static readonly List<Vector3>
        vector3Buffer1 = new List<Vector3>(),
        vector3Buffer2 = new List<Vector3>();
    private static readonly List<Vector2>
        uvs1 = new List<Vector2>(),
        uvs2 = new List<Vector2>();
    private static readonly List<int>
        triangles1 = new List<int>(),
        triangles2 = new List<int>();

    public static bool operator ==(MeshChunk x, MeshChunk y)
    {
        bool match =
            x.right == y.right &&
            x.left == y.left &&
            x.up == y.up &&
            x.down == y.down &&
            x.forward == y.forward &&
            x.groundedDirections == y.groundedDirections &&
            x.mesh.vertexCount == y.mesh.vertexCount &&
            x.mesh.bounds == y.mesh.bounds;

        if (match == false)
            return false;

        x.mesh.GetVertices(vector3Buffer1);
        y.mesh.GetVertices(vector3Buffer2);
        if (CompareVector3Lists(vector3Buffer1, vector3Buffer2) == false)
            return false;

        x.mesh.GetNormals(vector3Buffer1);
        y.mesh.GetNormals(vector3Buffer2);
        if (CompareVector3Lists(vector3Buffer1, vector3Buffer2) == false)
            return false;

        /*
        x.mesh.GetUVs(0, uvs1);
        y.mesh.GetUVs(0, uvs2);
        if (CompareVector2Lists(uvs1, uvs2) == false)
            return false;
        */

        x.mesh.GetTriangles(triangles1, 0);
        y.mesh.GetTriangles(triangles2, 0);
        if (CompareIntLists(triangles1, triangles2) == false)
            return false;

        return true;
    }
    public static bool operator !=(MeshChunk x, MeshChunk y)
    {
        return (x == y) == false;
    }

    private static bool CompareVector3Lists(List<Vector3> x, List<Vector3> y)
    {
        if (x.Count != y.Count)
            return false;
        for (int i = 0; i < x.Count; ++i)
            if (x[i] != y[i])
                return false;
        return true;        
    }
    private static bool CompareVector2Lists(List<Vector2> x, List<Vector2> y)
    {
        if (x.Count != y.Count)
            return false;
        for (int i = 0; i < x.Count; ++i)
            if (x[i] != y[i])
                return false;
        return true;
    }
    private static bool CompareIntLists(List<int> x, List<int> y)
    {
        if (x.Count != y.Count)
            return false;
        for (int i = 0; i < x.Count; ++i)
            if (x[i] != y[i])
                return false;
        return true;
    }

    public override bool Equals(object obj)
    {
        if (obj != null && obj is MeshChunk)
            return this == (MeshChunk)obj;
        else
            return false;
    }
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}