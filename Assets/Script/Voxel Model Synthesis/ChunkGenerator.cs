﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Threading.Tasks;
using static OrthogonalDirection;

public class ChunkGenerator : MonoBehaviour
{
    /// <summary>
    /// reduces the chances of stack-overflow.
    /// </summary>
    private class RecursivePayload
    {
        public int[,,] occupied;
        public bool[,,] mask;
        public System.Random randomGenerator;
        public int iterations;
    }
    private RecursivePayload CreatePayload(bool[,,] mask)
    {
        return new RecursivePayload()
        {
            occupied = CreateOccupied(),
            mask = mask,
            randomGenerator = new System.Random(Random.Range(int.MinValue, int.MaxValue)),
            iterations = default
        };
    }
    private void ResetPayload(RecursivePayload payload)
    {
        SetOccupiedUndecided(payload.occupied);
        payload.iterations = default;
    }

    private class GenerateParallelCache
    {
        public int processors;
        public RecursivePayload[] payloads;
        public List<int>[] allPossibleChunks;
        public List<Vector3Int>[] allAddedPositions;
        public bool[,,] mask;

        // thread locks
        public object resultProcessorLock;
        public object failureProcessorLock;
        public object seedAttemptsOverrunCountLock;

        // seeding cache
        public List<Vector3Int>[] allFailurePositions;

        // results
        public bool isFinished;
        public int[,,] resultOccupied;
        public void PropagateOccupied(int resultProcessor)
        {
            int[,,] resultOccupied = payloads[resultProcessor].occupied;
            List<Vector3Int> resultAdded = allAddedPositions[resultProcessor];
            for (int processor = 0; processor < processors; ++processor)
            {
                if (resultProcessor == processor)
                    continue;
                int[,,] occupied = payloads[processor].occupied;
                for (int i = 0; i < resultAdded.Count; ++i)
                {
                    Vector3Int position = resultAdded[i];
                    occupied[position.z, position.y, position.x] =
                        resultOccupied[position.z, position.y, position.x];
                }
            }
        }
        public void PropagateFailures()
        {
            for (int processor = 0; processor < processors; ++processor)
            {
                int[,,] occupied = payloads[processor].occupied;
                for (int otherProcessor = 0; otherProcessor < allFailurePositions.Length; ++otherProcessor)
                {
                    if (processor == otherProcessor)
                        continue;
                    List<Vector3Int> failurePositions = allFailurePositions[otherProcessor];
                    for (int i = 0; i < failurePositions.Count; ++i)
                    {
                        Vector3Int position = failurePositions[i];
                        occupied[position.z, position.y, position.x] = occupiedEmpty;
                    }
                }
            }
        }
        public bool TryGetResults(out int[,,] resultOccupied)
        {
            if (isFinished)
            {
                resultOccupied = this.resultOccupied;
                this.resultOccupied = null;
                return true;
            }
            else
            {
                resultOccupied = null;
                return false;
            }
        }
        public void Stop(int maxIterations)
        {
            if (isFinished)
                return;

            for (int processor = 0; processor < processors; ++processor)
            {
                lock(payloads[processor])
                {
                    payloads[processor].iterations = maxIterations;
                }
            }

            while (isFinished == false) ;
        }
    }

    /// <summary>
    /// Only call from Unity main thread!
    /// </summary>
    /// <param name="reserveDefaultThreads">Enable to run Unity in parallel without stuttering.</param>
    private GenerateParallelCache CreateParallelCache(bool reserveDefaultThreads = false)
    {
        GenerateParallelCache cache = new GenerateParallelCache();

        cache.processors = reserveDefaultThreads ?
            Mathf.Max(1, System.Environment.ProcessorCount - 2) : // reverse 2 threads for Unity's default threads.
            System.Environment.ProcessorCount;
        cache.payloads = new RecursivePayload[cache.processors];
        cache.allPossibleChunks = new List<int>[cache.processors];
        cache.allAddedPositions = new List<Vector3Int>[cache.processors];
        cache.mask = CreateMask();
        cache.allFailurePositions = new List<Vector3Int>[cache.processors];
        for (int processor = 0; processor < cache.processors; ++processor)
        {
            cache.payloads[processor] = CreatePayload(cache.mask);
            cache.allPossibleChunks[processor] = new List<int>(inputChunks.Count);
            cache.allAddedPositions[processor] = new List<Vector3Int>();
            cache.allFailurePositions[processor] = new List<Vector3Int>();
        }

        cache.resultProcessorLock = new object();
        cache.failureProcessorLock = new object();
        cache.seedAttemptsOverrunCountLock = new object();
        
        cache.isFinished = true;
        cache.resultOccupied = null;

        return cache;
    }
    /// <summary>
    /// Only call from Unity main thread!
    /// </summary>
    private void ResetParallelCache(GenerateParallelCache cache)
    {
        UpdateMask(cache.mask);
        for (int processor = 0; processor < cache.processors; ++processor)
        {
            ResetPayload(cache.payloads[processor]);
            cache.allPossibleChunks[processor].Clear();
            cache.allAddedPositions[processor].Clear();
            cache.allFailurePositions[processor].Clear();
        }

        cache.isFinished = true;
        cache.resultOccupied = null;
    }

    private struct OccupiedPacket
    {
        public Vector3Int position;
        public Mesh mesh;
    }

    [Header("References")]
    public ChunkPool chunkPool;

    [Header("Chunks Data")]
    public List<MeshChunk> inputChunks;
    public List<ChunkConnection> chunkConnections;

    [Header("Generator Settings")]
    [Tooltip("If true, attempts to populates all empty spaces in output. Otherwise, sows some seeds randomly.")]
    public bool fillCompletely;
    public Vector3Int outputDimensions;
    [Tooltip("Number of steps to backtrack.")]
    public int backtrackAmount = 3;
    [Tooltip("Possibility of backtracking when possibility is reduced.")]
    public float backtrackProbability = 1;
    [Tooltip("Max iterations before forced restart.")]
    public int maxIterations = 10000;

    [Header("Seeding Settings")]
    [Tooltip("Number of random starts.")]
    public int seeds = 1;
    public int maxSeedAttempts = 1000;
    [Tooltip("Max restarts before error.")]
    public int maxRestarts = 100;

    public enum MaskAxes { XY, XZ, YZ };
    [Header("Constraints")]
    [Tooltip("Texture is mapped linearly to the entire output space. Texture's greyscale controls the probability of output position being used (white:=possible,black:=impossible). Set to null to disable mask.")]
    public Texture2D maskTexture;
    [Tooltip("The 2 axes that correspond to the Texture2D maskTexture's 2 dimensions.")]
    public MaskAxes maskAxes = MaskAxes.XZ;
    [Tooltip("Which directions should the ground be? Grounded directions will force chunks to be in the extremes of that direction.")]
    public MultiDirection groundedDirections;

    private const int
        occupiedUndecided = -1,
        occupiedEmpty = -2;
    public string Generate()
    {
        ClampOutputDimensions();
        RecursivePayload payload = CreatePayload(CreateMask());
        List<int> possibleChunks = new List<int>();
        List<Vector3Int> addedPositions = new List<Vector3Int>();
        string returnMessage = string.Empty;

        if (fillCompletely)
        {
            for (int z = 0; z < outputDimensions.z; ++z)
                for (int y = 0; y < outputDimensions.y; ++y)
                    for (int x = 0; x < outputDimensions.x; ++x)
                        if (payload.occupied[z, y, x] == occupiedUndecided)
                        {
                            int restarts = 0;
                            // only iterate once, doesn't make much a difference in results, but speeds up substantially
                            StartGenerate(payload, new Vector3Int(x, y, z), possibleChunks, addedPositions, ref restarts);
                        }
        }
        else
        {
            int failedSeeds = 0;
            bool seedAttemptsOverrun = false;
            for (int seed = 0; seed < seeds; ++seed)
            {
                int restarts = 0;
                bool doLoop = true;
                do
                {
                    Vector3Int randomStart = CreateRandomStart(payload.occupied, null, out seedAttemptsOverrun);
                    if (seedAttemptsOverrun)
                        break;
                    switch (StartGenerate(payload, randomStart, possibleChunks, addedPositions, ref restarts))
                    {
                        case generateFailure:
                            // we continue if returnCode == generateFailure, because next iteration will have different randomStart.
                            break;
                        case generateSuccess:
                            doLoop = false;
                            break;
                        case generateIterationOverrun:
                            break;
                        case generateRestartOverrun:
                            doLoop = false;
                            ++failedSeeds;
                            break;
                        case generateBacktrackOverrun:
                            // we continue.
                            break;
                        default:
                            throw new System.Exception("Unknown return code from StartGenerate");
                    }
                }
                while (doLoop);

                if (seedAttemptsOverrun)
                {
                    failedSeeds += seeds - seed;
                    break;
                }
            }

            if (0 < failedSeeds)
            {
                if (seedAttemptsOverrun)
                    returnMessage = string.Format("Reached max seed attempts, failed seeds = {0}.", failedSeeds);
                else
                    returnMessage = string.Format("Failed seeds = {0}.", failedSeeds);
            }
        }

        SetChunkPool(payload.occupied);
        return returnMessage;
    }
    public void ClampOutputDimensions()
    {
        outputDimensions.x = outputDimensions.x < 1 ? 1 : outputDimensions.x;
        outputDimensions.y = outputDimensions.y < 1 ? 1 : outputDimensions.y;
        outputDimensions.z = outputDimensions.z < 1 ? 1 : outputDimensions.z;
    }
    /// <summary>
    /// Requires outputDimensions to be clamped, by calling ClampOutputDimensions().
    /// </summary>
    private int[,,] CreateOccupied()
    {
        // square 3d array of occupied positions. int represents index of inputChunks list.
        // -1 := undecided, -2 := definitely empty
        int[,,] occupied = new int[outputDimensions.z, outputDimensions.y, outputDimensions.x];
        SetOccupiedUndecided(occupied);
        return occupied;
    }
    private void SetOccupiedUndecided(int[,,] occupied)
    {
        for (int z = 0; z < occupied.GetLength(0); ++z)
            for (int y = 0; y < occupied.GetLength(1); ++y)
                for (int x = 0; x < occupied.GetLength(2); ++x)
                    occupied[z, y, x] = occupiedUndecided;
    }
    private IEnumerable<OccupiedPacket> UnpackOccupied(int[,,] occupied)
    {
        if (occupied == null)
            yield break;

        for (int z = 0; z < occupied.GetLength(0); ++z)
            for (int y = 0; y < occupied.GetLength(1); ++y)
                for (int x = 0; x < occupied.GetLength(2); ++x)
                    if (-1 < occupied[z, y, x])
                        yield return new OccupiedPacket()
                        {
                            position = new Vector3Int(x, y, z), 
                            mesh = inputChunks[occupied[z, y, x]].mesh 
                        };
    }
    /// <summary>
    /// Requires outputDimensions to be clamped, by calling ClampOutputDimensions().
    /// </summary>
    private bool[,,] CreateMask()
    {
        // square 3d array of positions that could take occupation.
        bool[,,] mask = new bool[outputDimensions.z, outputDimensions.y, outputDimensions.x];
        UpdateMask(mask);
        return mask;
    }
    private void UpdateMask(bool[,,] mask)
    {
        for (int z = 0; z < outputDimensions.z; ++z)
            for (int y = 0; y < outputDimensions.y; ++y)
                for (int x = 0; x < outputDimensions.x; ++x)
                    mask[z, y, x] = IsPositionMasked(new Vector3Int(x, y, z));
    }
    private bool IsPositionMasked(Vector3Int position)
    {
        if (maskTexture == null)
            return true;

        float greyScale;
        switch (maskAxes)
        {
            case MaskAxes.XY:
                greyScale = maskTexture.GetPixel(
                    position.x * maskTexture.width / outputDimensions.x,
                    position.y * maskTexture.height / outputDimensions.y, 0).grayscale;
                break;
            case MaskAxes.XZ:
                greyScale = maskTexture.GetPixel(
                    position.x * maskTexture.width / outputDimensions.x,
                    position.z * maskTexture.height / outputDimensions.z, 0).grayscale;
                break;
            case MaskAxes.YZ:
                greyScale = maskTexture.GetPixel(
                    position.y * maskTexture.width / outputDimensions.y,
                    position.z * maskTexture.height / outputDimensions.z, 0).grayscale;
                break;
            default:
                throw new System.Exception("IsPositionMasked: unknown maskAxes " + maskAxes);
        }
        if (Mathf.Approximately(greyScale, 0.0f))
            return false;
        else if (Mathf.Approximately(greyScale, 1.0f))
            return true;
        else
            return Random.value < greyScale;
    }

    private const int
        generateRestartOverrun = 3,
        generateBacktrackOverrun = 4;
    private int StartGenerate(
        RecursivePayload payload,
        Vector3Int startPosition,
        List<int> possibleChunks,
        List<Vector3Int> addedPositions,
        ref int restarts)
    {
        restarts++;
        if (maxRestarts < restarts)
            return generateRestartOverrun;

        possibleChunks.Clear();
        for (int i = 0; i < inputChunks.Count; ++i)
            possibleChunks.Add(i);
        addedPositions.Clear();
        payload.iterations = 0;
        int returnCode = RecursiveGenerate(payload, startPosition, possibleChunks, addedPositions);
        if (returnCode == generateFailure)
            // contradiction reached. this position must be empty
            payload.occupied[startPosition.z, startPosition.y, startPosition.x] = occupiedEmpty;
        else if (returnCode < 0)
            // backtracked too far
            return generateBacktrackOverrun;
        return returnCode;
    }
    private Vector3Int CreateRandomStart(int[,,] occupied, System.Random randomGenerator, out bool seedAttemptsOverrun)
    {
        int seedAttempt = 0;
        while (true)
        {
            ++seedAttempt;
            Vector3Int randomStart = randomGenerator == null ?
                new Vector3Int(
                    Random.Range(0, outputDimensions.x),
                    Random.Range(0, outputDimensions.y),
                    Random.Range(0, outputDimensions.z)) :
                new Vector3Int(
                    randomGenerator.Next(0, outputDimensions.x),
                    randomGenerator.Next(0, outputDimensions.y),
                    randomGenerator.Next(0, outputDimensions.z));

            if (occupied[randomStart.z, randomStart.y, randomStart.x] == occupiedUndecided)
            {
                seedAttemptsOverrun = false;
                return randomStart;
            }
            else
            {
                if (maxSeedAttempts < seedAttempt)
                {
                    seedAttemptsOverrun = true;
                    return default;
                }
            }
        }
    }

    private const int
        generateFailure = 0,
        generateSuccess = 1,
        generateIterationOverrun = 2;
    private int RecursiveGenerate(
        RecursivePayload payload,
        Vector3Int position,
        List<int> possibleChunks,
        List<Vector3Int> addedPositions)
    {
        if (payload.mask[position.z, position.y, position.x] == false)
            return generateFailure;

        lock (payload)
        {
            ++payload.iterations;
        }
        if (maxIterations < payload.iterations)
            return generateIterationOverrun;
        
        int[,,] occupied = payload.occupied;
        float originalPossibleCount = possibleChunks.Count;
        while (0 < possibleChunks.Count)
        {            
            int pickRandom = payload.randomGenerator.Next(0, possibleChunks.Count);
            int chunkIndex = possibleChunks[pickRandom];
            possibleChunks.RemoveAt(pickRandom);

            if (groundedDirections != MultiDirection.none &&
                inputChunks[chunkIndex].CheckGrounded(
                groundedDirections,
                position.x == 0, position.x == occupied.GetLength(2) - 1,
                position.y == 0, position.y == occupied.GetLength(1) - 1,
                position.z == 0, position.z == occupied.GetLength(0) - 1) == false)
                continue;

            bool isPossible = true;
            // check current occupied positions satisfy selected chunk constraints
            for (int direction = 0; direction < (int)Direction.MAX; ++direction)
            {
                if (TryGetOccupied(occupied, position + DirectionToVector(direction), out int occupiedIndex) &&
                    0 <= occupiedIndex)
                {
                    int connectionIndex = inputChunks[chunkIndex].GetConnectionIndex(direction);
                    int occupiedConnectionIndex = inputChunks[occupiedIndex].GetConnectionIndex(OppositeDirection(direction));

                    if ((connectionIndex == -1 && occupiedConnectionIndex == -1) ||
                        (connectionIndex == occupiedConnectionIndex))
                        continue;
                    else
                    {
                        isPossible = false;
                        break;
                    }
                }
            }

            if (isPossible == false)
            {
                if (payload.randomGenerator.Next(1000) < 1000 * backtrackProbability * (1 - possibleChunks.Count / originalPossibleCount))
                    return -backtrackAmount - 1;
                else
                    continue;
            }

            int returnCode = 0;
            // collapse possibilites
            occupied[position.z, position.y, position.x] = chunkIndex;
            List<Vector3Int> newAddedPositions = new List<Vector3Int>((int)Direction.MAX);
            for (int direction = 0; direction < (int)Direction.MAX; ++direction)
            {
                int connectionIndex = inputChunks[chunkIndex].GetConnectionIndex(direction);
                if (connectionIndex == -1)
                    continue;

                Vector3Int adjacentPosition = position + DirectionToVector(direction);

                if (TryGetOccupied(occupied, adjacentPosition, out int occupiedIndex) == false ||
                    occupiedIndex == occupiedEmpty)
                {
                    // out of bounds or forced empty
                    isPossible = false;
                    break;
                }

                if (occupiedIndex == occupiedUndecided)
                {
                    if (generateSuccess == (returnCode = RecursiveGenerate(
                            payload,
                            adjacentPosition,
                            new List<int>(chunkConnections[connectionIndex].chunks),
                            newAddedPositions)))
                        newAddedPositions.Add(adjacentPosition);
                    else
                    {
                        isPossible = false;
                        break;
                    }
                }
            }

            if (isPossible)
            {
                addedPositions.Add(position);
                for (int i = 0; i < newAddedPositions.Count; ++i)
                    addedPositions.Add(newAddedPositions[i]);
                return generateSuccess;
            }
            else
            {
                // Contradiction reached. Remove all added positions.
                occupied[position.z, position.y, position.x] = occupiedUndecided;
                for (int i = 0; i < newAddedPositions.Count; ++i)
                    occupied[newAddedPositions[i].z, newAddedPositions[i].y, newAddedPositions[i].x] =
                        occupiedUndecided;

                if (returnCode < -1)
                    return returnCode + 1; // less than -1 means to backtrack more
                else if (returnCode == generateIterationOverrun)
                    return generateIterationOverrun; // overrun means to exit completely
            }
        }
        return generateFailure;
    }
    private bool TryGetOccupied(int[,,] occupied, Vector3Int position, out int occupiedIndex)
    {
        if (0 <= position.z && position.z < occupied.GetLength(0) &&
            0 <= position.y && position.y < occupied.GetLength(1) &&
            0 <= position.x && position.x < occupied.GetLength(2))
        {
            occupiedIndex = occupied[position.z, position.y, position.x];
            return true;
        }
        else
        {
            occupiedIndex = default;
            return false;
        }
    }
    public string GenerateParallel()
    {
        GenerateParallelCache cache = CreateParallelCache();
        GenerateParallelThreadSafe(cache);
        if (cache.TryGetResults(out int[,,] resultOccupied))
        {
            SetChunkPool(resultOccupied);
            return parallelReturnMessage;
        }
        else
            return "Generate Parallel didn't work ...";
    }
    private string parallelReturnMessage;
    private void GenerateParallelThreadSafe(object cacheObject)
    {
        GenerateParallelCache cache = (GenerateParallelCache)cacheObject;
        cache.isFinished = false;
        parallelReturnMessage = string.Empty;

        int resultProcessor = -1,
            lastValidResult = -1,
            maxIterationsCopy = maxIterations;        

        if (fillCompletely)
        {
            int failureProcessor = -1;
            int z, y, x;

            void Work(int processor, ParallelLoopState loopState)
            {
                int restarts = 0;
                // only iterate once, doesn't make much a difference in results, but speeds up substantially
                switch (StartGenerate(
                    cache.payloads[processor], new Vector3Int(x, y, z),
                    cache.allPossibleChunks[processor], cache.allAddedPositions[processor], ref restarts))
                {
                    case generateFailure:
                        lock (cache.failureProcessorLock)
                        {
                            if (failureProcessor == -1)
                            {
                                // cause all other threads to quit.
                                maxIterations = 0;
                                loopState.Break();
                                failureProcessor = processor;
                            }
                        }
                        break;
                    case generateSuccess:
                        lock (cache.resultProcessorLock)
                        {
                            if (resultProcessor == -1)
                            {
                                // cause all other threads to quit.
                                maxIterations = 0;
                                loopState.Break();
                                resultProcessor = processor;
                            }
                        }
                        break;
                    default:
                        // do nothing else.
                        break;
                }
            }

            for (z = 0; z < outputDimensions.z; ++z)
                for (y = 0; y < outputDimensions.y; ++y)
                    for (x = 0; x < outputDimensions.x; ++x)
                        // result is propagated to all occupiedMatrices,
                        // so all of them have the same data at this point in time.
                        if (cache.payloads[0].occupied[z, y, x] == occupiedUndecided)
                        {
                            Parallel.For(0, cache.processors, Work);

                            if (failureProcessor != -1)
                            {
                                for(int processor = 0; processor < cache.processors; ++processor)
                                {
                                    if (failureProcessor == processor)
                                        continue;
                                    cache.payloads[processor].occupied[z, y, x] = occupiedEmpty;
                                }
                            }
                            else if (resultProcessor != -1)
                            {
                                lastValidResult = resultProcessor;
                                // if this is not the last for-loop iteration
                                if (!(z == outputDimensions.z - 1 &&
                                     y == outputDimensions.y - 1 &&
                                     x == outputDimensions.x - 1))
                                    cache.PropagateOccupied(resultProcessor);
                            }
                            // reset states
                            failureProcessor = -1;
                            resultProcessor = -1;
                            maxIterations = maxIterationsCopy;
                        }
        }
        else
        {
            int failedSeeds = 0, seedAttemptsOverrunCount = 0;

            void Work(int processor, ParallelLoopState loopState)
            {
                int restarts = 0;
                bool doLoop = true;
                cache.allFailurePositions[processor].Clear();
                do
                {
                    Vector3Int randomStart = CreateRandomStart(cache.payloads[processor].occupied, cache.payloads[processor].randomGenerator, out bool seedAttemptsOverrun);
                    if (seedAttemptsOverrun)
                    {
                        lock (cache.seedAttemptsOverrunCountLock)
                        {
                            ++seedAttemptsOverrunCount;
                        }
                        return;
                    }
                    switch (StartGenerate(
                        cache.payloads[processor], randomStart,
                        cache.allPossibleChunks[processor], cache.allAddedPositions[processor], ref restarts))
                    {
                        case generateFailure:
                            cache.allFailurePositions[processor].Add(randomStart);
                            break;
                        case generateSuccess:
                            lock (cache.resultProcessorLock)
                            {
                                if (resultProcessor == -1)
                                {
                                    // cause all other threads to quit.
                                    maxIterations = 0;
                                    loopState.Break();
                                    resultProcessor = processor;
                                }
                            }
                            return;
                        case generateIterationOverrun:
                            // check if result has been achieved.
                            if (resultProcessor != -1)
                                return;
                            break;
                        case generateRestartOverrun:
                            doLoop = false;
                            break;
                        case generateBacktrackOverrun:
                            break;
                        default:
                            throw new System.Exception("Unknown return code from StartGenerate");
                    }
                }
                while (doLoop);
            }

            for (int seed = 0; seed < seeds; ++seed)
            {
                Parallel.For(0, cache.processors, Work);

                if (resultProcessor == -1)
                {
                    if (seedAttemptsOverrunCount == cache.processors)
                    {
                        failedSeeds += seeds - seed;
                        break;
                    }
                    else
                        ++failedSeeds;
                }
                else
                {
                    lastValidResult = resultProcessor;
                    // if this is not the last seed iteration
                    if (seed + 1 < seeds)
                    {
                        cache.PropagateOccupied(resultProcessor);
                        cache.PropagateFailures();
                    }
                }
                // reset states
                resultProcessor = -1;
                maxIterations = maxIterationsCopy;
            }

            if (lastValidResult != -1 && 0 < failedSeeds)
            {
                if (seedAttemptsOverrunCount == cache.processors)
                    parallelReturnMessage = string.Format("All threads reached max seed attempts, Failed seeds = {0}", failedSeeds);
                else
                    parallelReturnMessage = string.Format("Failed seeds = {0}", failedSeeds);
            }
        }
        
        if (lastValidResult == -1)
        {
            parallelReturnMessage = string.Format("All threads failed. No solution found.");
            cache.resultOccupied = null;
        }
        else
            cache.resultOccupied = cache.payloads[lastValidResult].occupied;
        cache.isFinished = true;
    }
    /// <summary>
    /// Not thread safe!
    /// </summary>
    /// <param name="occupied"></param>
    private void SetChunkPool(int[,,] occupied)
    {
        chunkPool.ResetPool();
        foreach(OccupiedPacket packet in UnpackOccupied(occupied))
            chunkPool.Append(packet.position, packet.mesh, false);
        chunkPool.Finish();
    }
    [ContextMenu("Compare Performance")]
    private void ComparePerformance()
    {
        System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();

        stopwatch.Start();
        Generate();
        stopwatch.Stop();

        long linearMs = stopwatch.ElapsedMilliseconds;
        Debug.LogFormat("Linear {0}ms", linearMs);

        stopwatch.Reset();
        stopwatch.Start();
        GenerateParallel();
        stopwatch.Stop();

        long parallelMs = stopwatch.ElapsedMilliseconds;
        Debug.LogFormat("Parallel {0}ms", parallelMs);
        Debug.LogFormat("Speedup {0}x", (linearMs/(double)parallelMs).ToString("#.##"));
    }

    [Header("Infinite Generation Demo Settings")]
    public GameObject normalCamera;
    public Transform panCamera;
    public float cameraSpeed;
    public float cameraAngle;
    public float cameraHeight;
    public float cameraForward;

    private float lastPosition;
    private int lastGenerationIndex;
    private GenerateParallelCache parallelCache;

    private void OnDisable()
    {
        StopInfiniteGenerationDemo();
    }

    public void StartInfiniteGenerationDemo()
    {        
        StopAllCoroutines();
        parallelCache = CreateParallelCache(true);
        lastPosition = 0;
        lastGenerationIndex = 0;
        normalCamera.SetActive(false);
        panCamera.gameObject.SetActive(true);
        // generate first field
        GenerateParallel();
        StartCoroutine(PanRoutine());
    }

    public void StopInfiniteGenerationDemo()
    {
        StopAllCoroutines();
        if (normalCamera != null)
            normalCamera.SetActive(true);
        if (panCamera != null)
            panCamera.gameObject.SetActive(false);
    }

    private IEnumerator PanRoutine()
    {
        while (true)
        {
            float currentPosition = lastPosition + cameraSpeed * Time.deltaTime;
            panCamera.position = new Vector3(outputDimensions.x * chunkPool.gridStep.x / 2.0f, cameraHeight, currentPosition + cameraForward);
            panCamera.rotation = Quaternion.Euler(cameraAngle, 0, 0);

            float generationDistance = outputDimensions.z * chunkPool.gridStep.z;
            float nextGenerationDistance = Mathf.Ceil(lastPosition / generationDistance) * generationDistance;
            if (nextGenerationDistance < currentPosition)
            {
                parallelCache.Stop(maxIterations);
                Thread thread = new Thread(GenerateParallelThreadSafe);
                thread.Start(parallelCache);
            }

            if (parallelCache.TryGetResults(out int[,,] occupied) && occupied != null)
            {
                chunkPool.RemoveRange(0, lastGenerationIndex);
                lastGenerationIndex = chunkPool.chunkPoolTail;
                foreach (OccupiedPacket packet in UnpackOccupied(occupied))
                    chunkPool.Append(packet.position, packet.mesh, false, false, new Vector3(0, 0, nextGenerationDistance));
                ResetParallelCache(parallelCache);
            }

            lastPosition = currentPosition;
            yield return null;
        }
    }    
}
