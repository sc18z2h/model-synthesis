﻿using System;
using UnityEngine;

public static class OrthogonalDirection
{
    public enum Axis { x, y, z, MAX };
    public enum Direction { right, left, up, down, forward, backward, MAX };

    [Flags]
    public enum MultiDirection : short
    {
        none = 0,
        positiveX = 1 << 0,
        negativeX = 1 << 1,
        positiveY = 1 << 2,
        negativeY = 1 << 3,
        positiveZ = 1 << 4,
        negativeZ = 1 << 5
    };

    public static Direction OppositeDirection(Direction direction)
    {
        return (Direction)OppositeDirection((int)direction);
    }
    public static int OppositeDirection(int direction)
    {
        return (direction % 2 == 0 ?
            direction + 1 :
            direction - 1);
    }
    public static Vector3Int DirectionToVector(Direction direction)
    {
        return DirectionToVector((int)direction);
    }
    public static Vector3Int DirectionToVector(int direction)
    {
        switch (direction)
        {
            case 0:
                return Vector3Int.right;
            case 1:
                return Vector3Int.left;
            case 2:
                return Vector3Int.up;
            case 3:
                return Vector3Int.down;
            case 4:
                return new Vector3Int(0, 0, 1);
            case 5:
                return new Vector3Int(0, 0, -1);
            default:
                throw new System.Exception("DirectionToVector : no such direction");
        }
    }
}
