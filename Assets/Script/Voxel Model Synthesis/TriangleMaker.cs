﻿using UnityEngine;

public class TriangleMaker : MonoBehaviour
{
    public MeshFilter target;
    [ContextMenu("Make Triangle")]
    private void MakeTriangle()
    {
        if (target != null)
        {
            Mesh newMesh = new Mesh();

            newMesh.name = "Test Triangle";
            newMesh.vertices = new Vector3[] { Vector3.zero, Vector3.right + Vector3.up, Vector3.right };
            newMesh.uv = new Vector2[] { Vector2.zero, Vector2.right, Vector2.right + Vector2.up };
            newMesh.triangles = new int[] { 0, 1, 2 };
            newMesh.RecalculateNormals();

            target.mesh = newMesh;
        }
    }
}
