﻿using System.Collections.Generic;
using UnityEngine;

public class ChunkPool : MonoBehaviour
{
    [Header("Pool Settings")]
    public Vector3 gridStep;
    public Vector3 spacing;
    public GameObject chunkPrefab;
    public List<MeshFilter> chunkPool;
    public int chunkPoolTail;

    [SerializeField]
    private Material[] currentMaterials;

    [ContextMenu("Update Current Materials")]
    private void UpdateCurrentMaterials()
    {
        ChangeMaterials(chunkPrefab.GetComponent<Renderer>());
    }
    public void ChangeMaterials(Renderer changeTo)
    {
        if (changeTo == null)
        {
            Debug.LogError("ChunkPool: cannot change materials to null renderer", this);
            return;
        }

        currentMaterials = changeTo.sharedMaterials;
        for (int i = 0; i < chunkPool.Count; ++i)
            chunkPool[i].GetComponent<Renderer>().sharedMaterials = currentMaterials;
    }
    public void SetParameters(Vector3 gridStep, Vector3 spacing)
    {
        this.gridStep = gridStep;
        this.spacing = spacing;
    }
    public void Append(Vector3 position, Mesh mesh, bool spaced = false, bool nameIndex = false, Vector3 offset = new Vector3())
    {
        GameObject chunkObject;
        if (chunkPool.Count <= chunkPoolTail)
        {
            chunkObject = Instantiate(chunkPrefab, transform);
            chunkObject.GetComponent<Renderer>().sharedMaterials = currentMaterials;
            chunkPool.Add(chunkObject.GetComponent<MeshFilter>());
        }
        else
        {
            chunkObject = chunkPool[chunkPoolTail].gameObject;
        }

        chunkObject.SetActive(true);
        chunkObject.name = "Chunk " + (nameIndex ? chunkPoolTail.ToString() : position.ToString());

        if (spaced)
            chunkObject.transform.localPosition = 
                Vector3.Scale(gridStep, position) +
                Vector3.Scale(spacing, position) +
                offset;
        else
            chunkObject.transform.localPosition =
                Vector3.Scale(gridStep, position) +
                offset;

        chunkPool[chunkPoolTail].mesh = mesh;
        ++chunkPoolTail;
    }
    public int Finish()
    {
        for (; chunkPoolTail < chunkPool.Count; ++chunkPoolTail)
            DeactiveItem(chunkPoolTail);
        return chunkPoolTail;
    }
    public void ResetPool()
    {
        bool isEditor = Application.isEditor;
        for (int i = 0; i < chunkPool.Count; ++i)
        {
            if (isEditor)
                DestroyImmediate(chunkPool[i].gameObject);
            else
                Destroy(chunkPool[i].gameObject);
        }
        chunkPool.Clear();
        chunkPoolTail = 0;
    }
    public void RemoveRange(int index, int count)
    {
        if (count == 0)
            return;

        chunkPool.Capacity = chunkPool.Count + count;
        int end = index + count;
        for (int i = index; i < end; ++i)
        {
            DeactiveItem(i);
            // add this item to the end of the list
            chunkPool.Add(chunkPool[i]);
        }
        chunkPool.RemoveRange(index, count);        
        chunkPoolTail -= count;
    }
    public Bounds GetBounds()
    {
        if (chunkPool.Count == 0)
            return new Bounds(gridStep / 2.0f, gridStep);
        
        Bounds bounds = chunkPool[0].sharedMesh.bounds;
        bounds.center += chunkPool[0].transform.position;
        for (int i = 1; i < chunkPoolTail; ++i)
        {
            Bounds addBounds = chunkPool[i].sharedMesh.bounds;
            addBounds.center += chunkPool[i].transform.position;
            bounds.Encapsulate(addBounds);
        }
        return bounds;
    }
    private void DeactiveItem(int index)
    {
        chunkPool[index].gameObject.SetActive(false);
        chunkPool[index].gameObject.transform.position = Vector3.zero;
        chunkPool[index].mesh = null;
    }
}
